const gulp = require("gulp");
const gulpWebpack = require("gulp-webpack");
const webpack = require("webpack");
const mergeStream = require("merge-stream");
var sourcemaps = require("gulp-sourcemaps");
const ts = require("gulp-typescript");

let env;
switch (process.env.NODE_ENV) {
  case "development":
    env = "dev";
    break;
  case "production":
    env = "prod";
    break;
  case "local":
    env = "local";
    break;
  default:
    env = "dev";
}

const webpackConfigFileLb = require(`./clientapp/webpack.${env}`);

gulp.task("build-client", function () {
  return gulp
    .src("./clientapp/src/index.js")
    .pipe(gulpWebpack(webpackConfigFileLb, webpack))
    .pipe(gulp.dest("dist/client"));
});

// gulp.task("build-sup", function () {
//   return gulp
//     .src("./sup/src/index.js")
//     .pipe(gulpWebpack(webpackConfigFileSup, webpack))
//     .pipe(gulp.dest("dist/sup"));
// });

gulp.task("build-server", function () {
  const tsProject = ts.createProject("./server/tsconfig.json");
  return gulp
    .src("./server/src/**/*.ts")
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("dist/server"));
});

gulp.task("copy-resources", function () {
  return mergeStream([
    gulp.src("./server/tsconfig.json").pipe(gulp.dest("dist/server")),
    gulp.src("./package.json").pipe(gulp.dest("dist/")),
  ]);
});

gulp.task(
  "default",
  gulp.parallel("build-client", "build-server", "copy-resources")
);
