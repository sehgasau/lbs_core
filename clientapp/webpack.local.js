var webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: ["babel-polyfill", "./src/index.js"],
  mode: "development",
  output: {
    path: path.join(__dirname, "/dist"),
    filename: "bundelLocal.[hash].js",
    publicPath: "/",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["react", "stage-0"],
          plugins: [
            "transform-class-properties",
            "transform-object-rest-spread",
            [
              "babel-plugin-import",
              {
                libraryName: "@material-ui/core",
                // Use "'libraryDirectory': ''," if your bundler does not support ES modules
                libraryDirectory: "",
                camel2DashComponentName: false,
              },
              "core",
            ],
            [
              "babel-plugin-import",
              {
                libraryName: "@material-ui/icons",
                // Use "'libraryDirectory': ''," if your bundler does not support ES modules
                libraryDirectory: "",
                camel2DashComponentName: false,
              },
              "icons",
            ],
          ],
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/,
        use: ["url-loader"],
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    compress: true,
    watchContentBase: true,
    liveReload: true,
    port: 8081,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": '"local"',
      "process.env.REACT_APP_API": '"http://localhost:3000/api/v1"',
      "process.env.EVENT_API_URL": '"http://localhost:3000/event"',
    }),
  ],
};
