import React, { useState, useEffect } from "react";
import {
  Route,
  Link,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import "./App.css";
import LandingPage from "./components/landingPage/landingPage";
import AdminPage from "./components/adminPage/adminPage";
import { useSelector } from "react-redux";
import AdminLogin from "./components/adminPage/adminLogin/adminLogin";
import store from "./global/store/store";
import actionCreators from "./global/actionCreator";
import Error from "./components/error";
import UserPortal from "./components/userPortal/userPortal";
import UserActivationPage from "./components/shared/UserActivationPage";
import SupWelcomePage from "./components/sup/event/welcomePage";
import EventRoom from "./components/sup/event/eventRoom";

function App(props) {
  const globalLoading = useSelector(
    (state) => state.globalProperties.globalLoading
  );

  //https://www.youtube.com/watch?v=z2XCUu2wIl0  - - - -   REDUX-THUNK

  const RootProtectedRoute = ({ component: Component, ...rest }) => {
    const user = useSelector((state) => state.userInfo);
    if (user) {
      if (user.role === "CLIENT_ADMIN") {
        return (
          <Route
            {...rest}
            render={(props) => <Redirect to={`/admin/console`} />}
          />
        );
      } else if (user.role === "CONSUMER") {
        return (
          <Route {...rest} render={(props) => <Redirect to={`/user`} />} />
        );
      } else {
        return <Route {...rest} render={(props) => <Redirect to={`/`} />} />;
      }
    } else {
      return <Route {...rest} render={(props) => <Component {...props} />} />;
    }
  };
  const AdminProtectedRoute = ({ component: Component, ...rest }) => {
    const user = useSelector((state) => state.userInfo);
    return (
      <Route
        {...rest}
        render={(props) =>
          user ? <Component {...props} /> : <Redirect to={`/login/admin`} />
        }
      />
    );
  };

  const AdminLoginProtectedRoute = ({ component: Component, ...rest }) => {
    const user = useSelector((state) => state.userInfo);
    return (
      <Route
        {...rest}
        render={(props) =>
          !user ? <Component {...props} /> : <Redirect to={`/admin/console`} />
        }
      />
    );
  };

  const ConsumerProtectedRoute = ({ component: Component, ...rest }) => {
    const user = useSelector((state) => state.userInfo);
    return (
      <Route
        {...rest}
        render={(props) =>
          user ? <Component {...props} /> : <Redirect to={`/`} />
        }
      />
    );
  };

  // REACT ROUTER NOTE:  USE EXACT KEYWORD FOR ONLY LAST CHILD ROUTES otherwise it will not match the route

  return (
    <React.Fragment>
      <Switch>
        <AdminLoginProtectedRoute
          exact
          path="/login/admin"
          component={AdminLogin}
        />
        <AdminProtectedRoute path="/admin/console" component={AdminPage} />
        <Route
          exact
          path="/user/activate/:userId"
          component={UserActivationPage}
        />
        <ConsumerProtectedRoute path="/user" component={UserPortal} />
        <Route exact path="/s/event/:id" component={EventRoom} />
        <Route exact path="/s/welcome" component={SupWelcomePage} />
        <RootProtectedRoute path="/" component={LandingPage} />
        <Route path="*" exact={true} component={Error} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
