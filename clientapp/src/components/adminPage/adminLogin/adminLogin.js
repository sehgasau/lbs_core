import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { useDispatch } from "react-redux";
import store from "../../../global/store/store";
import actionCreator from "../../../global/actionCreator";
import CircularProgress from "@material-ui/core/CircularProgress";
import { purple } from "@material-ui/core/colors";
import Snackbar from "@material-ui/core/Snackbar";

import {
  Route,
  Link,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";

const useStyles = makeStyles({
  root: {
    maxWidth: 430,
    height: 600,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  loginButton: {
    width: "100%",
  },
  wrapper: {
    margin: 3,
    position: "relative",
  },
  buttonProgress: {
    color: purple[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
});

function AdminLogin(props) {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [checked, setChecked] = useState(false);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState({ val: false, message: "" });
  const dispatch = useDispatch();

  useEffect(() => {
    if (
      localStorage.getItem("adminemail") !== null &&
      localStorage.getItem("adminpassword") !== null
    ) {
      setEmail(localStorage.getItem("adminemail").toString());
      setPassword(localStorage.getItem("adminpassword").toString());
      console.log("EMAIl ADMIN : ", email);
      console.log("PASSWORD ADMIN : ", password);
      setChecked(true);
    }
    store.dispatch(actionCreator.tryAutoAuthentication("ADMIN"));
  }, []);

  const handelInput = (event, type) => {
    if (type === "email") {
      setEmail(event.target.value);
    } else {
      setPassword(event.target.value);
    }
  };

  const handleChecked = () => {
    if (
      !checked &&
      email.toString().trim() !== "" &&
      password.toString().trim() !== ""
    ) {
      localStorage.setItem("adminemail", email);
      localStorage.setItem("adminpassword", password);
      setChecked(true);
    } else if (
      checked ||
      email.toString().trim() === "" ||
      password.toString().trim() === ""
    ) {
      localStorage.removeItem("adminemail");
      localStorage.removeItem("adminpassword");
      setChecked(false);
    }
  };

  const auth = async () => {
    //store.dispatch(actions.globalPropertiesActions._startGlobalLoading())
    setLoading(true);
    const result = await store.dispatch(
      actionCreator.authenticate({ email, password, isAdminLoginAttempt: true })
    );
    if (!result || result === "Please activate your account first") {
      setOpen({
        val: true,
        message: result ? result : "Email or password is invalid",
      });
      setLoading(false);
    }
  };

  return (
    <Box
      style={{ backgroundColor: "#F5F5F5", height: "100vh" }}
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Snackbar
        open={open.val}
        onClose={() => {
          setOpen(false);
        }}
        message={open.message}
      />
      <Card className={classes.root}>
        <CardMedia
          style={{
            width: "100%",
            height: "20%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={require("../../../assets/images/logo.png")}
            style={{ height: "90%", width: "60%", marginTop: "1%" }}
          />
        </CardMedia>
        <CardContent
          style={{
            display: "flex",
            width: "80%",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Typography style={{ color: "#545454" }} variant="h3">
            Admin Login
          </Typography>
          <TextField
            onChange={(event) => handelInput(event, "email")}
            style={{
              backgroundColor: "#E8F0FE",
              width: "100%",
              marginTop: "5%",
            }}
            id="outlined-basic"
            label="Email"
            variant="outlined"
            value={email}
          />
          <TextField
            onChange={(event) => handelInput(event, "password")}
            style={{
              backgroundColor: "#E8F0FE",
              width: "100%",
              marginTop: "6%",
            }}
            id="outlined-basic"
            label="Password"
            type="password"
            variant="outlined"
            value={password}
          />
          <FormControlLabel
            control={
              <Checkbox
                name="checked"
                color="primary"
                checked={checked}
                onChange={handleChecked}
              />
            }
            label="Save Credentials"
          />
        </CardContent>

        <CardActions>
          <div className={classes.wrapper}>
            <Button
              disabled={loading}
              variant="contained"
              color="primary"
              className={classes.loginButton}
              onClick={() => auth()}
            >
              Login
            </Button>
            {loading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </CardActions>
        <Link style={{ marginTop: "4%" }} to="/">
          Go to liteboards.com
        </Link>
      </Card>
    </Box>
  );
}

export default AdminLogin;
