// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState } from "react";
import {
  Route,
  Link,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import userActions from "../../../global/actions/userActions";
import store from "../../../global/store/store";
import actionCreator from "../../../global/actionCreator";

// UI IMPORTS -----------------------------------------------------------------------------------
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles, useTheme } from "@material-ui/core/styles";

// COMPONENT IMPORTS ----------------------------------------------------------------------------
import PortalProgressBar from "../../shared/portalProgressBar";
import AdminNav from "./adminNav/adminNav";
import UserPage from "./userPage/userPage";
import ClientPage from "./clientPage/ClientPage";

// ----------------------------------------------------------------------------------------------

const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function AdminConsole(props) {
  const globalLoading = useSelector(
    (state) => state.globalProperties.globalLoading
  );
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AdminNav />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {globalLoading ? <PortalProgressBar variant="indeterminate" /> : ""}
        <Switch>
          <Route exact path="/admin/console/users/:role" component={UserPage} />
          <Route exact path="/admin/console/clients" component={ClientPage} />
        </Switch>
        {/* routes will go here */}
      </main>
    </div>
  );
}

export default AdminConsole;
