// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import {
  Route,
  Link,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import actions from "../../../../global/actions/allActions";
import store from "../../../../global/store/store";
import actionCreator from "../../../../global/actionCreator";

// UI IMPORTS -----------------------------------------------------------------------------------
import AppBar from "@material-ui/core/AppBar";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MailIcon from "@material-ui/icons/Mail";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Avatar from "@material-ui/core/Avatar";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import NotificationsIcon from "@material-ui/icons/Notifications";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import ViewListIcon from "@material-ui/icons/ViewList";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Accordion from "@material-ui/core/Accordion";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import PeopleOutlineIcon from "@material-ui/icons/PeopleOutline";
import Collapse from "@material-ui/core/Collapse";

// COMPONENT IMPORTS ----------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------

const drawerWidth = 270;
const appBarHeight = 100;
const useStyles = makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      height: `${appBarHeight}`,
      marginLeft: drawerWidth,
    },
    backgroundColor: "#fafafa",
    color: "black",
    boxShadow: "none",
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#2e2f36",
    color: "white",
  },
  toolbar: theme.mixins.toolbar,
  link: {
    textDecoration: "none",
  },
  avatar: {
    height: "45px",
    width: "45px",
    marginRight: "2%",
  },
  notMenuIcon: {
    color: "#ffc700",
    height: "35px",
    width: "35px",
  },
  notIconButton: {
    marginRight: "1%",
  },
  listItem: {
    backgroundColor: "#595959",
    height: 70,
  },
  dropList: {
    backgroundColor: "#595959",
    height: 70,
  },
}));

function AdminNav(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [bgColor, setBgColor] = useState("");
  const [usersSubBg, setUsersSubBg] = useState("");
  const [usersCollapse, setUsersCollapse] = useState(false);
  const [navLinks, setNavLinks] = useState([
    {
      label: "Reports",
      icon: <EqualizerIcon style={{ color: "#f8faca" }} />,
      iconColor: "#f8faca",
      to: "/admin/console",
    },
    {
      label: "Users",
      icon: <FormatListBulletedIcon style={{ color: "#c1f0c0" }} />,
      iconColor: "#c1f0c0",
      to: "/admin/console/users",
    },
    {
      label: "Clients",
      icon: <ViewListIcon style={{ color: "#b6f2ec" }} />,
      iconColor: "#b6f2ec",
      to: "/admin/console/clients",
    },
  ]);
  const _dispatch = useDispatch();
  const _userInfo = useSelector((store) => store.userInfo);
  const logout = () => {
    //dispatch(userActions._auth());
    //props.history.push('/login/admin')
    _dispatch(actionCreator.logout("ADMIN"));
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const changeBgColor = (val, label) => {
    setBgColor({
      label: label,
      val: val,
    });
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {navLinks.map((linkObj) => {
          let linkReturn = (
            <Link
              style={{ color: "white" }}
              className={classes.link}
              to={linkObj.to}
              onClick={() => {
                changeBgColor("#79a5ed", linkObj.label);
                setUsersCollapse(false);
                setUsersSubBg("");
              }}
            >
              <ListItem
                button
                className={classes.listItem}
                style={{
                  backgroundColor:
                    bgColor["label"] === linkObj.label
                      ? bgColor["val"]
                      : "#595959",
                }}
              >
                <ListItemIcon>{linkObj.icon}</ListItemIcon>
                <ListItemText primary={linkObj.label} />
              </ListItem>
            </Link>
          );
          if (linkObj.label === "Clients" && _userInfo.role !== "SUPER_ADMIN") {
            return "";
          }
          if (linkObj.label === "Users") {
            linkReturn = (
              <Link style={{ color: "white" }} className={classes.link}>
                <ListItem
                  button
                  className={classes.dropList}
                  style={{
                    backgroundColor:
                      bgColor["label"] === linkObj.label
                        ? bgColor["val"]
                        : "#595959",
                  }}
                  onClick={() => {
                    changeBgColor("#413d45", linkObj.label);
                    setUsersCollapse(!usersCollapse);
                  }}
                >
                  <ListItemIcon>{linkObj.icon}</ListItemIcon>
                  <ListItemText primary={linkObj.label} />
                  <ExpandMoreIcon />
                </ListItem>
                <Collapse in={usersCollapse}>
                  <Link
                    style={{ color: "white" }}
                    className={classes.link}
                    to={linkObj.to + "/admins"}
                  >
                    <ListItem
                      button
                      className={classes.dropList}
                      style={{
                        backgroundColor:
                          bgColor["label"] === linkObj.label
                            ? bgColor["val"]
                            : "#595959",
                        backgroundColor:
                          usersSubBg["label"] === "Admins"
                            ? usersSubBg["val"]
                            : "#595959",
                      }}
                      onClick={() => {
                        setUsersSubBg({
                          val: "#1d355c",
                          label: "Admins",
                        });
                      }}
                    >
                      <ListItemIcon>
                        <PeopleOutlineIcon style={{ color: "#00ffa2" }} />
                      </ListItemIcon>
                      <ListItemText primary="Admins" />
                    </ListItem>
                  </Link>
                  <Link
                    style={{ color: "white" }}
                    className={classes.link}
                    to={linkObj.to + "/employees"}
                  >
                    <ListItem
                      button
                      className={classes.dropList}
                      style={{
                        backgroundColor:
                          bgColor["label"] === linkObj.label
                            ? bgColor["val"]
                            : "#595959",
                        backgroundColor:
                          usersSubBg["label"] === "Employees"
                            ? usersSubBg["val"]
                            : "#595959",
                      }}
                      onClick={() => {
                        setUsersSubBg({
                          val: "#1d355c",
                          label: "Employees",
                        });
                      }}
                    >
                      <ListItemIcon>
                        <SupervisorAccountIcon style={{ color: "#00ffa2" }} />
                      </ListItemIcon>
                      <ListItemText primary="Employees" />
                    </ListItem>
                  </Link>
                </Collapse>
              </Link>
            );
          }
          return linkReturn;
        })}
      </List>
      <Divider />
    </div>
  );

  return (
    <React.Fragment>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h4" noWrap>
            Admin Console - {_userInfo.client}{" "}
            {_userInfo.role === "SUPER_ADMIN" ? "- Super Admin" : ""}
          </Typography>
          <Box style={{ flexGrow: 1 }}></Box>

          <Avatar alt="Travis Howard" src="" className={classes.avatar} />
          <IconButton className={classes.notIconButton}>
            <NotificationsIcon className={classes.notMenuIcon} />
          </IconButton>
          <IconButton onClick={() => logout()} style={{ color: "black" }}>
            <ExitToAppIcon /> Logout
          </IconButton>
        </Toolbar>
      </AppBar>

      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </React.Fragment>
  );
}

export default AdminNav;
