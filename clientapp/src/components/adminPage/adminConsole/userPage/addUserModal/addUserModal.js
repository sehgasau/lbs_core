// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import * as axios from "axios";
import { useDispatch, connect, useSelector } from "react-redux";
import Axios from "../../../../../config/axiosConfig";
// UI IMPORTS -----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

// COMPONENT IMPORTS ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    height: "500px",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
    [theme.breakpoints.down("md")]: {
      width: "60%",
    },
    width: "25%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  textField: {
    width: "80%",
    marginTop: "3%",
  },
  loginButton: {
    backgroundColor: "#808BEC",
    color: "#FFFFFF",
    fontSize: "15px",
    width: "80%",
    height: "10%",
    marginTop: "3%",
  },
}));

const AddUserModal = (props) => {
  const classes = useStyles();
  const _dispatch = useDispatch();
  const _userInfo = useSelector((store) => store.userInfo);
  const [role, setRole] = useState("CLIENT_ADMIN");
  const [name, setName] = useState("");
  const [workEmail, setWorkEmail] = useState("");
  const [clientName, setClientName] = useState("");
  const [errorPop, setErrorPop] = useState(false);
  const handleAdminTypeChange = (e) => {
    setRole(e.target.value);
  };

  const addUser = () => {
    const payload = {
      workEmail: workEmail,
      name: name,
      clientName:
        _userInfo.role === "CLIENT_ADMIN" || _userInfo.role === "BRANCH_ADMIN"
          ? _userInfo.client
          : clientName.name,
      branchName: null,
      role: props.userType === "Admin" ? role : "CONSUMER",
    };
    Axios._post(`/user/add-user`, payload, true)
      .then((res) => {
        setErrorPop({
          message: "User successfully added",
          type: "success",
          val: true,
        });
      })
      .catch((err) => {
        setErrorPop({
          message: "Unable to insert user",
          type: "error",
          val: true,
        });
      });
  };

  return (
    <React.Fragment>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        open={errorPop.val}
        autoHideDuration={3000}
        onClose={(e, reason) => {
          setErrorPop({
            val: false,
          });
        }}
        message="I love snacks"
      >
        <Alert severity={errorPop.type}>{errorPop.message}</Alert>
      </Snackbar>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.open}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Slide direction="up" in={props.open} mountOnEnter unmountOnExit>
          <div className={classes.paper}>
            <Typography variant="h3" style={{ marginTop: "5%" }}>
              Add an {props.userType}
            </Typography>
            <Autocomplete
              id="combo-box-demo"
              options={
                _userInfo.clients ? _userInfo.clients : [_userInfo.client]
              }
              getOptionLabel={(option) =>
                _userInfo.clients ? option.name : option
              }
              autoSelect={true}
              autoComplete={true}
              autoHighlight={true}
              // value={clientName}
              onChange={(e, newValue) => {
                setClientName({ name: newValue.name ? newValue.name : null });
              }}
              className={classes.textField}
              disabled={_userInfo.role === "SUPER_ADMIN" ? false : true}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={
                    _userInfo.role === "SUPER_ADMIN"
                      ? "Client"
                      : `Client - ${_userInfo.client}`
                  }
                  variant="outlined"
                  // value={clientName}
                  // onChange={(e) => {
                  //   setClientName({ name: e.target.value });
                  // }}
                />
              )}
            />
            <TextField
              className={classes.textField}
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
              id="outlined-basic"
              label="Name"
              variant="outlined"
              autoFocus={true}
            />
            <TextField
              className={classes.textField}
              value={workEmail}
              onChange={(e) => {
                setWorkEmail(e.target.value);
              }}
              id="outlined-basic"
              label="Work Email"
              type="email"
              variant="outlined"
            />

            {props.userType !== "Admin" ? (
              ""
            ) : (
              <FormControl className={classes.textField} variant="filled">
                <InputLabel id="demo-simple-select-label">
                  Admin Type
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={role}
                  onChange={handleAdminTypeChange}
                >
                  <MenuItem value="CLIENT_ADMIN">Client Admin</MenuItem>
                  <MenuItem value="BRANCH_ADMIN">Branch Admin</MenuItem>
                </Select>
              </FormControl>
            )}
            <Button
              onClick={() => {
                addUser();
                //console.log(clientName);
              }}
              variant="contained"
              color="primary"
              className={classes.loginButton}
            >
              Add
            </Button>
          </div>
        </Slide>
      </Modal>
    </React.Fragment>
  );
};

export default AddUserModal;
