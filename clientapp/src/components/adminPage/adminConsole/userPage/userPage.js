// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import * as axios from "axios";
import { useDispatch, connect, useSelector } from "react-redux";
import Axios from "../../../../config/axiosConfig";

// UI IMPORTS -----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

// COMPONENT IMPORTS ----------------------------------------------------------------------------
import AddUserModal from "./addUserModal/addUserModal";
import DataSheet from "../../../shared/DataSheet";

// ----------------------------------------------------------------------------------------------
const columns = [
  { id: "name", label: "Name", minWidth: 100 },
  { id: "workEmail", label: "Work Email", minWidth: 80 },
  {
    id: "designation",
    label: "Designation",
    minWidth: 100,
    align: "center",
  },
  {
    id: "client",
    label: "Client",
    minWidth: 100,
    align: "center",
  },
  {
    id: "branch",
    label: "Branch",
    minWidth: 100,
    align: "center",
  },
  {
    id: "isActivated",
    label: "Activated",
    minWidth: 100,
    align: "center",
  },
  {
    id: "edit",
    label: "Action",
    minWidth: 100,
    align: "center",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 600,
  },
  searchName: {
    marginRight: "1%",
  },
  box: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "flex-start",
  },
  filterPaper: {
    width: "100%",
    backgroundColor: "#f5f5f5",
    marginBottom: "1.5%",
    padding: "0.5%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  formControl: {
    minWidth: 180,
    marginRight: "1%",
  },
  check: {
    color: "green",
  },
  cross: {
    color: "red",
  },
  editIcon: {
    color: "blue",
  },
}));

function UserPage(props) {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [branches, setBranches] = useState([""]);
  const [branchName, setBranchName] = useState("");
  const [userName, setUserName] = useState("");
  const [isActivated, setIsActivated] = useState(null);
  const [users, setUsers] = useState([]);
  const [selectedClient, setSelectedClient] = useState(null);
  const [limit, setLimit] = useState(rowsPerPage);
  const [offset, setOffset] = useState(0);
  const [rowsCount, setRowsCount] = useState(0);
  const _dispatch = useDispatch();
  const _userInfo = useSelector((store) => store.userInfo);
  const [addUserModalOpen, setAddUserModalOpen] = React.useState(false);
  const handleOpen = () => {
    setAddUserModalOpen(true);
  };

  const handleClose = () => {
    setAddUserModalOpen(false);
  };
  const handleChangePage = (event, newPage) => {
    const newOffset = calculateOffset(newPage);
    setPage(newPage);
    setOffset(newOffset);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setLimit(+event.target.value);
    handleChangePage(event, 0);
  };

  const calculateOffset = (page) => {
    const offset = page * limit;
    return offset;
  };

  const requestData = () => {
    const options = {
      isActivated: isActivated,
      branchName: branchName,
      cName:
        _userInfo.role === "SUPER_ADMIN" ? selectedClient : _userInfo.client,
      userName: userName,
      limit: limit,
      offset: offset,
      role:
        props.match.params.role === "employees" ? "CONSUMER" : "CLIENT_ADMIN",
    };
    Axios._get(`/user/usersByOptions`, options, true)
      .then((res) => {
        const { users } = res.data;
        setUsers(users);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleIsActivatedChange = (e) => {
    setIsActivated(e.target.value);
    handleChangePage(e, 0);
  };

  const handleBranchChange = (e) => {
    setBranchName(e.target.value);
    handleChangePage(e, 0);
  };

  const handleInputChange = (e) => {
    setUserName(e.target.value);
  };

  const handleClientChange = (e) => {
    setSelectedClient(e.target.value);
    handleChangePage(e, 0);
  };

  const requestBranches = (client) => {
    Axios._get(`/branch/branches/${client}`, null, true).then((res) => {
      const { branches } = res.data;
      branches.map((branchObj) => {
        setBranches((branches) => [...branches, branchObj.name]);
      });
    });
  };

  useEffect(() => {
    setBranches([""]);
    setBranchName("");
    requestData();
    if (selectedClient !== null) {
      requestBranches(selectedClient);
    }
  }, [selectedClient]);

  useEffect(() => {
    if (_userInfo.role !== "SUPER_ADMIN") requestBranches(_userInfo.client);
  }, []);

  useEffect(() => {
    requestData();
  }, [branchName, isActivated, userName, limit, page, props.match.params.role]);

  return (
    <React.Fragment>
      <Paper className={classes.filterPaper}>
        <Box className={classes.box}>
          <TextField
            id="outlined-basic"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
            variant="filled"
            placeholder="user's name"
            className={classes.searchName}
            value={userName}
            onChange={handleInputChange}
          />

          <FormControl
            className={classes.formControl}
            variant="filled"
            disabled={(() => {
              if (_userInfo.role !== "SUPER_ADMIN") {
                return false;
              } else {
                if (selectedClient === null) {
                  return true;
                } else {
                  return false;
                }
              }
            })()}
          >
            <InputLabel id="demo-simple-select-label">Branch</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={branchName}
              onChange={handleBranchChange}
            >
              {branches.map((branchName) => {
                return (
                  <MenuItem
                    value={branchName.trim() === "" ? null : branchName}
                  >
                    {branchName.trim() === "" ? "None" : branchName}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>

          <FormControl className={classes.formControl} variant="filled">
            <InputLabel id="demo-simple-select-label">
              Activation Status
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={isActivated}
              onChange={handleIsActivatedChange}
            >
              <MenuItem value={null}>None</MenuItem>
              <MenuItem value={true}>Activated</MenuItem>
              <MenuItem value={false}>Not Activated</MenuItem>
            </Select>
          </FormControl>

          {_userInfo.role === "SUPER_ADMIN" ? (
            <FormControl className={classes.formControl} variant="filled">
              <InputLabel id="demo-simple-select-label">Client</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectedClient}
                onChange={handleClientChange}
              >
                <MenuItem value={null}>None</MenuItem>
                {_userInfo.clients.map((clientObj) => {
                  return (
                    <MenuItem value={clientObj.name}>{clientObj.name}</MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          ) : (
            ""
          )}
        </Box>
        <Button
          variant="contained"
          color="primary"
          size="small"
          className={classes.addButton}
          onClick={() => {
            handleOpen();
          }}
        >
          Add User
        </Button>
      </Paper>
      <DataSheet
        users={users}
        type="users"
        columns={columns}
        page={page}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rowsPerPage={rowsPerPage}
        dataSetSize={users.length}
        limit={limit}
      />
      <AddUserModal
        userType={
          props.match.params.role !== "employees" ? "Admin" : "Employee"
        }
        open={addUserModalOpen}
        handleClose={handleClose}
      />
    </React.Fragment>
  );
}

export default UserPage;
