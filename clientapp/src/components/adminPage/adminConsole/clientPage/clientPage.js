// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import { useDispatch, connect, useSelector } from "react-redux";
import Axios from "../../../../config/axiosConfig";
// UI IMPORTS -----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

// COMPONENT IMPORTS ----------------------------------------------------------------------------
import DataSheet from "../../../shared/DataSheet";
import AddClientModal from "./addClientModal/addClientModal";

// ----------------------------------------------------------------------------------------------
const columns = [
  { id: "name", label: "Name", minWidth: 100 },
  { id: "industryType", label: "Industry", minWidth: 80 },
  {
    id: "subscriptionType",
    label: "Subscription Type",
    minWidth: 100,
    align: "center",
  },
  {
    id: "creationDate",
    label: "Date Created (yyyy/mm/dd)",
    minWidth: 100,
    align: "center",
  },
  {
    id: "edit",
    label: "Action",
    minWidth: 100,
    align: "center",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 600,
  },
  searchName: {
    marginRight: "1%",
  },
  box: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "flex-start",
  },
  filterPaper: {
    width: "100%",
    backgroundColor: "#f5f5f5",
    marginBottom: "1.5%",
    padding: "0.5%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  formControl: {
    minWidth: 180,
    marginRight: "1%",
  },
  editIcon: {
    color: "blue",
  },
}));

function ClientPage(props) {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [limit, setLimit] = useState(rowsPerPage);
  const [offset, setOffset] = useState(0);
  const [rowsCount, setRowsCount] = useState(0);
  const [clientName, setClientName] = useState("");
  const [clients, setClients] = useState([]);
  const [subscriptionType, setSubscriptionType] = useState("");
  const [addClientModalOpen, setAddClientModalOpen] = useState(false);
  const _dispatch = useDispatch();
  const _userInfo = useSelector((store) => store.userInfo);

  const handleChangePage = (event, newPage) => {
    const newOffset = calculateOffset(newPage);
    setPage(newPage);
    setOffset(newOffset);
  };
  const handleOpen = () => {
    setAddClientModalOpen(true);
  };

  const handleClose = () => {
    setAddClientModalOpen(false);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setLimit(+event.target.value);
    handleChangePage(event, 0);
  };

  const calculateOffset = (page) => {
    const offset = page * limit;
    return offset;
  };

  const requestData = () => {
    const options = {
      clientName: clientName,
      limit: limit,
      offset: offset,
    };
    Axios._get(`/client/clientByOptions`, options, true)
      .then((res) => {
        const { clients } = res.data;
        setClients(clients);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleInputChange = (e) => {
    setClientName(e.target.value);
  };

  useEffect(() => {
    setClients(_userInfo.clients);
  }, [clientName]);

  //   useEffect(() => {
  //     requestData();
  //   }, [clientName, limit, page]);
  const changeClientNameFilter = (client) => {
    return client.name.toLowerCase().indexOf(clientName.toLowerCase()) !== -1;
  };

  const changeSubscriptionFilter = (client) => {
    if (subscriptionType === "") {
      return true;
    } else {
      return client.subscriptionType === subscriptionType;
    }
  };
  const handleSubscriptionTypeChange = (e) => {
    setSubscriptionType(e.target.value);
  };
  return (
    <React.Fragment>
      {_userInfo.role !== "SUPER_ADMIN" ? (
        props.history.push("/admin/console")
      ) : (
        <React.Fragment>
          <Paper className={classes.filterPaper}>
            <Box className={classes.box}>
              <TextField
                id="outlined-basic"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
                placeholder="client name"
                className={classes.searchName}
                value={clientName}
                onChange={handleInputChange}
              />
              <FormControl className={classes.formControl} variant="filled">
                <InputLabel id="demo-simple-select-label">
                  Subscription Type
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={subscriptionType}
                  onChange={handleSubscriptionTypeChange}
                >
                  <MenuItem value="">None</MenuItem>
                  <MenuItem value="PREMIUM">PREMIUM</MenuItem>
                  <MenuItem value="BASIC">BASIC</MenuItem>
                  <MenuItem value="FREE TIER">FREE TIER</MenuItem>
                </Select>
              </FormControl>
            </Box>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className={classes.addButton}
              onClick={() => {
                handleOpen();
              }}
            >
              Add Client
            </Button>
          </Paper>
          <DataSheet
            clients={clients}
            type="clients"
            columns={columns}
            page={page}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
            rowsPerPage={rowsPerPage}
            dataSetSize={clients.length}
            limit={limit}
            changeSubscriptionFilter={changeSubscriptionFilter}
            changeClientNameFilter={changeClientNameFilter}
          />
        </React.Fragment>
      )}
      <AddClientModal open={addClientModalOpen} handleClose={handleClose} />
    </React.Fragment>
  );
}

export default ClientPage;
