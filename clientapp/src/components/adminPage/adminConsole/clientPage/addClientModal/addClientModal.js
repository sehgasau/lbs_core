// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import { useDispatch, connect, useSelector } from "react-redux";
import Axios from "../../../../../config/axiosConfig";
import actionCreator from "../../../../../global/actionCreator";

// UI IMPORTS -----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

// COMPONENT IMPORTS ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    height: "500px",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
    [theme.breakpoints.down("md")]: {
      width: "60%",
    },
    width: "25%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  textField: {
    width: "80%",
    marginTop: "3%",
  },
  loginButton: {
    backgroundColor: "#808BEC",
    color: "#FFFFFF",
    fontSize: "15px",
    width: "80%",
    height: "10%",
    marginTop: "3%",
  },
}));

const AddClientModal = (props) => {
  const classes = useStyles();
  const _userInfo = useSelector((store) => store.userInfo);
  const _dispatch = useDispatch();
  const [subsType, setSubsType] = useState("FREE TIER");
  const [industry, setIndustry] = useState("");

  /**
   * @todo create industry entity in db and fetch all the industries at the time of login and store in cache cause this is not frequent
   */
  const [industryList, setIndustryList] = useState([
    "Agriculture",
    "Banking",
    "IT",
  ]);

  const [clientName, setClientName] = useState("");
  const [errorPop, setErrorPop] = useState(false);
  const handleSubsTypeChange = (e) => {
    setSubsType(e.target.value);
  };

  const addClient = () => {
    if (
      clientName.trim() === "" ||
      industry.trim() === "" ||
      subsType.trim() === ""
    ) {
      setErrorPop({
        message: "Please fill in the required fields",
        type: "error",
        val: true,
      });
      return;
    }

    const payload = {
      name: clientName,
      industry: industry,
      subsType: subsType,
    };
    Axios._post(`/client/add-client`, payload, true)
      .then((res) => {
        const client = res.data.client;
        const updatedUserState = {
          ..._userInfo,
          clients: [..._userInfo.clients, client],
        };
        _dispatch(actionCreator.updateUserState(updatedUserState));
        setErrorPop({
          message: "Client successfully added",
          type: "success",
          val: true,
        });
      })
      .catch((err) => {
        setErrorPop({
          message: "Unable to insert client",
          type: "error",
          val: true,
        });
      });
  };

  return (
    <React.Fragment>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        open={errorPop.val}
        autoHideDuration={3000}
        onClose={(e, reason) => {
          setErrorPop({
            val: false,
          });
        }}
        message="I love snacks"
      >
        <Alert severity={errorPop.type}>{errorPop.message}</Alert>
      </Snackbar>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.open}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Slide direction="up" in={props.open} mountOnEnter unmountOnExit>
          <div className={classes.paper}>
            <Typography variant="h3" style={{ marginTop: "5%" }}>
              Add a Client
            </Typography>
            <Autocomplete
              id="combo-box-demo"
              options={industryList}
              getOptionLabel={(option) => option}
              autoSelect={true}
              autoComplete={true}
              autoHighlight={true}
              // value={clientName}
              onChange={(e, newValue) => {
                setIndustry(newValue);
              }}
              className={classes.textField}
              disabled={_userInfo.role === "SUPER_ADMIN" ? false : true}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={"Industry *"}
                  variant="outlined"
                  // value={clientName}
                  // onChange={(e) => {
                  //   setClientName({ name: e.target.value });
                  // }}
                />
              )}
            />
            <TextField
              className={classes.textField}
              value={clientName}
              onChange={(e) => {
                setClientName(e.target.value);
              }}
              id="outlined-basic"
              label="Client Name *"
              variant="outlined"
              autoFocus={true}
            />
            <FormControl className={classes.textField} variant="filled">
              <InputLabel id="demo-simple-select-label">
                Subscription Type *
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={subsType}
                onChange={handleSubsTypeChange}
              >
                <MenuItem value="PREMIUM">Premium</MenuItem>
                <MenuItem value="BASIC">Basic</MenuItem>
                <MenuItem value="FREE TIER">Free Tier</MenuItem>
              </Select>
            </FormControl>

            <Button
              onClick={() => {
                addClient();
                //console.log(clientName);
              }}
              variant="contained"
              color="primary"
              className={classes.loginButton}
            >
              Add
            </Button>
          </div>
        </Slide>
      </Modal>
    </React.Fragment>
  );
};

export default AddClientModal;
