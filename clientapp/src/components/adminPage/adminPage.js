import React, { useState } from "react";
import {
  Route,
  Link,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import AdminConsole from "./adminConsole/adminConsole";
import Error from "../error";

function AdminPage(props) {
  return (
    <React.Fragment>
      <Switch>
        <Route path="/admin/console" component={AdminConsole} />
        <Route component={Error} />
        {/* <Route exact path="/developer" component={DeveloperPage}/>
     <Route exact path="/usage" component={UsagePage}/>
     <Route exact path="/about" component={AboutPage}/>
     <Route exact path="/upComing" component={UpComingPage}/> */}
      </Switch>
    </React.Fragment>
  );
}

export default AdminPage;
