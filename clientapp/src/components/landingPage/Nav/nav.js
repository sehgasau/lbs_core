import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import styled from 'styled-components';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Hidden from '@material-ui/core/Hidden';

export default function Nav(props){
    const StyledAppBar = styled(AppBar)`
    box-shadow: none;
    background: #fcfcfc;
    `;
    const style={
        bulb:{
            width:'5%',
            height:'5%',
            margin:'1%'
          },
          navLinks:{
            marginLeft:'2%',
            width:'100%'
          },
        button:{
          fontSize:'18px',
          color:'#4d4d4d',
          marginLeft:'3%'
        },
      }
    const ResponsiveMenu = (props)=>{
        return(
          <Menu
              id="simple-menu"
              anchorEl={props.anchorEl}
              keepMounted
              open={Boolean(props.anchorEl)}
              onClose={props.handleClose}
            >
              <MenuItem onClick={props.handleClose}>Home</MenuItem>
              <MenuItem onClick={props.handleClose}>About</MenuItem>
              <MenuItem onClick={props.handleClose}>Features</MenuItem>
              <MenuItem onClick={props.handleClose}>Pricing</MenuItem>
              <MenuItem onClick={props.handleClose}>Contact</MenuItem>
            </Menu>
        )
      }

    const [anchorEl, setAnchorEl] = useState(null);
    const handleClose = () => {
      setAnchorEl(null);
    };

    return(
        <React.Fragment>
          <Hidden smUp implementation="css">
            <IconButton  color="inherit" aria-label="menu" onClick={()=>setAnchorEl(true)}>
              <MenuIcon />
            </IconButton>
          </Hidden>
            {
                anchorEl ? <ResponsiveMenu anchorEl={anchorEl} handleClose={handleClose}/> : ''
            }
            <Hidden smDown implementation="css">
              <StyledAppBar position="static" >
              <Toolbar >
                  <img src={require('../../../assets/images/bulb.png')} style={style.bulb} />
                  <div style={style.navLinks}>
                  <Button style={style.button}>Home</Button>
                  <Button style={style.button}>About</Button> 
                  <Button style={style.button}>Features</Button>
                  <Button style={style.button}>Pricing</Button>
                  <Button style={style.button}>Contact</Button>
                  {console.log(props.width)}
                  </div>
              </Toolbar>
              </StyledAppBar>
            </Hidden>
        </React.Fragment>
    )
}

