import React, { Component } from "react";
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import Splash from "./splash/splash";
import Nav from "./Nav/nav";
import withWidth from "@material-ui/core/withWidth";
import Error from "../error";
import About from "./About/aboutPage";

function LandingPage(props) {
  return (
    <Grid container>
      <Grid item md={12} xs={12} lg={12} xl={12} sm={12}>
        <Nav width={props.width} />
      </Grid>
      <Grid item md={12} xs={12} lg={12} xl={12} sm={12}>
        <Switch>
          <Route exact path="/" component={Splash} />
          {/*<Route exact path="/about" component={About}/>
                <Route component={Error}/>                
                 <Route exact path="/developer" component={DeveloperPage}/>
                <Route exact path="/usage" component={UsagePage}/>
                <Route exact path="/about" component={AboutPage}/>
                <Route exact path="/upComing" component={UpComingPage}/> */}
        </Switch>
      </Grid>
    </Grid>
  );
}
LandingPage.propTypes = {
  width: PropTypes.oneOf(["lg", "md", "sm", "xl", "xs"]).isRequired,
};

export default withWidth()(LandingPage);
