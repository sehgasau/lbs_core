// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import store from "../../../../global/store/store";
import actionCreator from "../../../../global/actionCreator";
import bcrypt from "bcryptjs";
import actionCreators from "../../../../global/actionCreator";
// UI IMPORTS -----------------------------------------------------------------------------------
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Snackbar from "@material-ui/core/Snackbar";

// COMPONENT IMPORTS ----------------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    height: "400px",
    width: "300px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  loginButton: {
    backgroundColor: "#808BEC",
    color: "#FFFFFF",
    fontSize: "15px",
    width: "80%",
    height: "12%",
    marginTop: "2%",
  },
  textField: {
    width: "80%",
    marginTop: "3%",
  },
  googleButton: {
    color: "#FFFFFF",
    fontSize: "15px",
    width: "80%",
    height: "12%",
    marginTop: "2%",
    marginBottom: "3%",
  },
}));

export default function Login(props) {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [checked, setChecked] = useState(false);
  const [open, setOpen] = useState({ val: false, message: "" });

  const auth = async () => {
    //store.dispatch(actions.globalPropertiesActions._startGlobalLoading())
    const result = await store.dispatch(
      actionCreator.authenticate({ email, password })
    );
    if (!result || result === "Please activate your account first") {
      setOpen({
        val: true,
        message: result ? result : "Email or password incorrect",
      });
    }
  };
  useEffect(() => {
    if (
      localStorage.getItem("email") !== null &&
      localStorage.getItem("password") !== null
    ) {
      setEmail(localStorage.getItem("email").toString());
      setPassword(localStorage.getItem("password").toString());
      setChecked(true);
    }
    store.dispatch(actionCreators.tryAutoAuthentication("CONSUMER"));
  }, []);

  const handelInput = (event, type) => {
    if (type === "email") {
      setEmail(event.target.value);
    } else {
      setPassword(event.target.value);
    }
  };

  const handleChecked = async () => {
    if (
      !checked &&
      email.toString().trim() !== "" &&
      password.toString().trim() !== ""
    ) {
      localStorage.setItem("email", email);
      localStorage.setItem("password", password);
      setChecked(true);
    } else if (
      checked ||
      email.toString().trim() === "" ||
      password.toString().trim() === ""
    ) {
      localStorage.removeItem("email");
      localStorage.removeItem("password");
      setChecked(false);
    }
  };

  return (
    <React.Fragment>
      <Snackbar
        open={open.val}
        onClose={() => {
          setOpen({ val: false, message: "" });
        }}
        message={open.message}
      />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.open}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={props.open}>
          <div className={classes.paper}>
            <Typography variant="h3">Login</Typography>

            <TextField
              className={classes.textField}
              value={email}
              onChange={(event) => handelInput(event, "email")}
              id="outlined-basic"
              label="Email"
              variant="outlined"
            />
            <TextField
              className={classes.textField}
              value={password}
              onChange={(event) => handelInput(event, "password")}
              id="outlined-basic"
              label="Password"
              type="password"
              variant="outlined"
            />
            <FormControlLabel
              control={
                <Checkbox
                  name="checked"
                  color="primary"
                  checked={checked}
                  onChange={handleChecked}
                />
              }
              label="Save Credentials"
            />
            <Button
              onClick={() => auth()}
              variant="contained"
              color="primary"
              className={classes.loginButton}
            >
              Login
            </Button>
            <hr
              style={{ width: "90%", height: "0.01px", marginTop: "4%" }}
            ></hr>
            <Button
              variant="contained"
              color="secondary"
              className={classes.googleButton}
            >
              Google
            </Button>
          </div>
        </Fade>
      </Modal>
    </React.Fragment>
  );
}
