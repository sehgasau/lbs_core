import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Login from "./login/login";
import Button from "@material-ui/core/Button";
import globalPropertiesActions from "../../../global/actions/globalPropertiesActions";
import store from "../../../global/store/store";

export default function Splash(props) {
  const style = {
    logo: {
      width: "340px",
      height: "150px",
      marginTop: "8%",
    },
    h3: {
      color: "#6766A5",
    },
    loginButton: {
      backgroundColor: "#C4C4C4",
      color: "#5A5EC3",
      fontSize: "15px",
      width: "10%",
      height: "100%",
      marginRight: "2%",
    },
    exploreButton: {
      backgroundColor: "#808BEC",
      color: "#FFFFFF",
      fontSize: "15px",
      width: "10%",
      height: "100%",
    },
  };

  const [loginOpen, setLoginOpen] = React.useState(false);
  const handleOpen = () => {
    // if(localStorage.getItem("email") !== null && localStorage.getItem("password") !== null){
    //   alert("Logged in")
    // }
    setLoginOpen(true);
  };

  const handleClose = () => {
    setLoginOpen(false);
  };
  return (
    <React.Fragment>
      <Box
        display="flex"
        justifyContent="center"
        flexDirection="column"
        alignItems="center"
      >
        {process.env.NODE_ENV === "development" ? (
          <div>
            <hr />
            <h1>DEVELOPMENT BRANCH</h1>
            <hr />
          </div>
        ) : (
          ""
        )}
        {process.env.NODE_ENV === "local" ? (
          <div>
            <hr />
            <h1>LOCAL BRANCH</h1>
            <hr />
          </div>
        ) : (
          ""
        )}
        <img
          src={require("../../../assets/images/logo.png")}
          style={style.logo}
        />
        <Typography style={{ ...style.h3, marginTop: "1%" }} variant="h3">
          Intelligent HR Management and
        </Typography>
        <Typography style={style.h3} variant="h3">
          Job search platform
        </Typography>
        <Box
          style={{ width: "100%", height: "60px", marginTop: "2%" }}
          display="flex"
          justifyContent="center"
        >
          <Button style={style.loginButton} onClick={() => handleOpen()}>
            <Typography variant="button">Login</Typography>
          </Button>
          <Button style={style.exploreButton}>
            <Typography variant="button">Explore</Typography>
          </Button>
        </Box>
      </Box>
      <Login open={loginOpen} handleClose={handleClose} />
    </React.Fragment>
  );
}
