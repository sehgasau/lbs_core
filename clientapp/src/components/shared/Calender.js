// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import axios from "axios";

// UI IMPORTS -----------------------------------------------------------------------------------
import { makeStyles, useTheme } from "@material-ui/core/styles";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";

// COMPONENT IMPORTS ----------------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  monthBar: {
    width: "100%",
    height: "40px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f7f7f7",
  },
  daysBar: {
    backgroundColor: "#972aeb",
    width: 100 / 7 + "%",
    height: "60px",
    textAlign: "center",
    color: "white",
    padding: "2%",
    fontSize: "20px",
    boxShadow: "0px 13px 10px #d9d9d9",
  },
}));
const Calender = (props) => {
  const classes = useStyles();
  const [matrix, setMatrix] = useState([]);
  const [_date, _setDate] = useState(new Date());
  const [month, setMonth] = useState(_date.getMonth());
  const [year, setYear] = useState(_date.getFullYear());
  const [days, setDays] = useState([
    "Mon",
    "Tue",
    "Wed",
    "Thr",
    "Fri",
    "Sat",
    "Sun",
  ]);
  const [months, setMonths] = useState([
    "January",
    "Feburary",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ]);
  const buildMatrix = () => {
    const currentMonth = months[_date.getMonth()];
    const currentYear = _date.getFullYear();
    const currentDate = _date.getDate();
    let startOfMonth = new Date(year, month, 1);
    let daysInCurrentMonth = new Date(year, month + 1, 0).getDate();
    let daysInPrevMonth = new Date(year, month, 0).getDate();
    let dayIndex = startOfMonth.getDay() - 1;
    let start = 1;
    let matrix = [];
    let check = true;
    for (let a = 0; a < 6; a++) {
      let row = [];
      for (let i = 0; i < 7; i++) {
        const payloadToPush = {
          date: 0,
          month: months[month],
          year: year,
          isCurrentDay: false,
          day: days[i],
          events: {},
          timeLog: {},
          color: "black",
        };
        if (a === 0 && check) {
          if (i === dayIndex) {
            payloadToPush.date = start;
            start++;
            check = false;
          } else {
            const valToPush = daysInPrevMonth - dayIndex + i + 1;
            payloadToPush.date = valToPush;
            payloadToPush.month = months[month - 1];
            payloadToPush.color = "#999999";
            if (month === 0) {
              payloadToPush.year = year - 1;
              payloadToPush.month = months[11];
            }
          }
        } else {
          if (start > daysInCurrentMonth) {
            const val = start - daysInCurrentMonth;
            payloadToPush.date = val;
            payloadToPush.month = months[month + 1];
            payloadToPush.color = "#999999";
            if (month === 11) {
              payloadToPush.year = year + 1;
              payloadToPush.month = months[0];
            }
          } else {
            payloadToPush.date = start;
          }
          start++;
        }
        if (
          payloadToPush.date == currentDate &&
          payloadToPush.year == currentYear &&
          payloadToPush.month == currentMonth
        ) {
          payloadToPush.isCurrentDay = true;
        }
        row.push(payloadToPush);
      }
      matrix.push(row);
    }
    return matrix;
  };

  useEffect(() => {
    setMatrix(buildMatrix());
  }, [month, year]);

  return (
    <Grid
      container
      fluid
      style={{ height: "500px", boxShadow: "14px 14px 50px #d9d9d9" }}
    >
      <Grid item className={classes.monthBar}>
        <IconButton
          style={{ marginRight: "3%" }}
          onClick={() => {
            if (month !== 0) setMonth(month - 1);
          }}
        >
          <ChevronLeftIcon style={{ height: "40px", width: "40px" }} />
        </IconButton>
        <Button>
          <Typography
            variant="button"
            style={{ color: "black", textAlign: "center" }}
          >
            {months[month]} | <span style={{ color: "red" }}>{year}</span>
          </Typography>
        </Button>
        <IconButton
          style={{ marginLeft: "3%" }}
          onClick={() => {
            if (month !== 11) setMonth(month + 1);
          }}
        >
          <ChevronRightIcon style={{ height: "40px", width: "40px" }} />
        </IconButton>
      </Grid>
      {days.map((day) => {
        return (
          <Grid item className={classes.daysBar}>
            <Typography
              variant="h4"
              style={{
                color: "white",
                textAlign: "center",
              }}
            >
              {day}
            </Typography>
          </Grid>
        );
      })}
      {matrix.map((row) => {
        return (
          <Grid container style={{ width: "100%" }}>
            {row.map((object) => {
              return (
                <Grid
                  item
                  style={{
                    width: 100 / 7 + "%",
                    height: "100%",
                    textAlign: "center",
                  }}
                >
                  <Button
                    onClick={() => {
                      alert(
                        `${object.date} ${object.month} ${object.year} ${object.day}`
                      );
                    }}
                    style={{
                      backgroundColor: object.isCurrentDay ? "#d1d1d1" : "",
                    }}
                  >
                    <Typography
                      variant="button"
                      style={{
                        color:
                          object.day === "Sat" || object.day === "Sun"
                            ? "red"
                            : !object.isCurrentDay
                            ? object.color
                            : "black",
                        textAlign: "center",
                      }}
                    >
                      {object.date}
                    </Typography>
                  </Button>
                </Grid>
              );
            })}
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Calender;
