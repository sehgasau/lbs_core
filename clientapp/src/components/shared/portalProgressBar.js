import { makeStyles, withStyles } from '@material-ui/core/styles';
import React, {useState,useEffect} from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const PortalProgressBar = withStyles((theme) => ({
    root: {
      height: 10,
    },
    colorPrimary: {
      backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
      background: 'linear-gradient(90deg, rgba(96,42,200,1) 0%, rgba(96,42,200,0.927608543417367) 8%, rgba(40,69,227,1) 66%, rgba(44,129,224,0.6110819327731092) 100%)',
    },
}))(LinearProgress);

export default PortalProgressBar;