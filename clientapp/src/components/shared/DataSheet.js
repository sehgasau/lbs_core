// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";

// UI IMPORTS -----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

// COMPONENT IMPORTS ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 600,
  },
  check: {
    color: "green",
  },
  cross: {
    color: "red",
  },
  editIcon: {
    color: "blue",
  },
}));

function DataSheet(props) {
  const classes = useStyles();
  const buildClientsSheet = () => {
    return props.clients
      .filter(props.changeSubscriptionFilter)
      .filter(props.changeClientNameFilter)
      .map((client) => {
        return (
          <TableRow hover role="checkbox" tabIndex={-1} key={client.name}>
            {props.columns.map((column) => {
              let value = client[column.id];
              if (column.id === "edit") {
                value = (
                  <IconButton>
                    <EditIcon className={classes.editIcon}></EditIcon>
                  </IconButton>
                );
              }
              if (column.id === "creationDate") {
                const date = new Date(client[column.id].toString());
                value = `${date.getFullYear()}/${date.getMonth()}/${date.getDate()}`;
              }
              return (
                <TableCell key={column.id} align={column.align}>
                  {(() => {
                    if (value === true) {
                      return <CheckIcon className={classes.check} />;
                    } else if (value === false) {
                      return <CloseIcon className={classes.cross} />;
                    } else return <Typography variant="h6">{value}</Typography>;
                  })()}
                </TableCell>
              );
            })}
          </TableRow>
        );
      });
  };

  const buildUsersSheet = () => {
    return props.users.map((user) => {
      return (
        <TableRow hover role="checkbox" tabIndex={-1} key={user.email}>
          {props.columns.map((column) => {
            let value = user[column.id];
            if (column.id === "edit" && !value) {
              value = (
                <IconButton>
                  <EditIcon className={classes.editIcon}></EditIcon>
                </IconButton>
              );
            }
            if (column.id === "branch" || column.id === "client") {
              value = user[column.id] ? user[column.id].name : "Not Assigned";
              //setBranches([...branches,value])
            }
            if (column.id === "designation") {
              value =
                user[column.id].trim() !== ""
                  ? user[column.id]
                  : "Not Assigned";
              //setBranches([...branches,value])
            }

            return (
              <TableCell key={column.id} align={column.align}>
                {(() => {
                  if (value === true) {
                    return <CheckIcon className={classes.check} />;
                  } else if (value === false) {
                    return <CloseIcon className={classes.cross} />;
                  } else return <Typography variant="h6">{value}</Typography>;
                })()}
              </TableCell>
            );
          })}
        </TableRow>
      );
    });
  };

  const generateTable = () => {
    switch (props.type) {
      case "clients":
        return buildClientsSheet();
        break;
      case "users":
        return buildUsersSheet();
        break;
    }
  };

  return (
    <React.Fragment>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {props.columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      backgroundColor: "#ebebeb",
                    }}
                  >
                    <Typography variant="h5" noWrap>
                      {column.label}
                    </Typography>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>{generateTable()}</TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={
            props.type !== "clients" ? [10, 30, 50] : [props.dataSetSize]
          }
          component="div"
          count={-1}
          rowsPerPage={props.rowsPerPage}
          page={props.page}
          onChangePage={props.handleChangePage}
          onChangeRowsPerPage={props.handleChangeRowsPerPage}
          nextIconButtonProps={{
            disabled:
              props.dataSetSize < props.limit || props.type === "clients"
                ? true
                : false,
          }}
          labelDisplayedRows={({ from, to, count }) => {
            if (props.dataSetSize < props.limit) {
              return `${from} - ${to} of ${
                props.page * props.limit + props.dataSetSize
              } results`;
            } else {
              return props.type !== "clients"
                ? `${from} - ${to} of more than ${to}`
                : `${props.dataSetSize} clients`;
            }
          }}
        />
        {props.dataSetSize === 0 ? (
          <Typography
            variant="h3"
            style={{ textAlign: "center", margin: "1%", padding: "2%" }}
          >
            No results found, try changing the filters : )
          </Typography>
        ) : (
          ""
        )}
      </Paper>
    </React.Fragment>
  );
}

export default DataSheet;
