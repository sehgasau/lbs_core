// REACT IMPORTS -------------------------------------------------------------------------------
import React, { useState, useEffect } from "react";
import Axios from "../../config/axiosConfig";
// UI IMPORTS -----------------------------------------------------------------------------------
import Box from "@material-ui/core/Box";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

// COMPONENT IMPORTS ----------------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "100vh",
    justifyContent: "center",
    alignItems: "center",
  },
  toolbar: theme.mixins.toolbar,
  card: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    [theme.breakpoints.up("sm")]: {
      width: "600px",
    },
    [theme.breakpoints.down("sm")]: {
      width: "80%",
    },
    height: "80%",
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.25)",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "7%",
  },
  button: {
    backgroundColor: "#7B54EC",
    color: "white",
    width: "60%",
  },
  cardAction: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
  },
  logo: {
    width: "130px",
    height: "130px",
  },
  h3: {
    marginTop: "8%",
  },
  h5: {
    marginTop: "5%",
  },
}));

export default function UserActivationPage(props) {
  const userId = props.match.params.userId;
  const [isAlreadyActivated, setIsAlreadyActivated] = useState(false);
  const [successMessage, setSuccessMessage] = useState(false);
  const [client, setClient] = useState("");
  const [userName, setUserName] = useState("");

  useEffect(() => {
    Axios._get(`/user/activation/status/${userId}`, null, false)
      .then((res) => {
        const { isActivated, client, userName } = res.data;
        setClient(client);
        setUserName(userName);
        if (isActivated) {
          setIsAlreadyActivated(isActivated);
        }
      })
      .catch((err) => {
        props.history.push("/");
      });
  }, []);

  const activateUser = () => {
    Axios._put(`/user/activation/update/${userId}`, null, false)
      .then((res) => {
        const { isActivated } = res.data;
        if (isActivated) {
          setSuccessMessage(true);
        }
      })
      .catch((err) => {
        alert("Ops some error");
      });
  };

  const classes = useStyles();

  return (
    <React.Fragment>
      {!isAlreadyActivated ? (
        <Box flexGrow={1} className={classes.root}>
          <CssBaseline />
          <Card className={classes.card}>
            <CardContent className={classes.content}>
              <img
                src={require("../../assets/images/bulb.png")}
                className={classes.logo}
              />
              <Typography variant="h3" className={classes.h3}>
                Hello {userName}!
              </Typography>
              {successMessage ? (
                <Typography variant="h5" className={classes.h5}>
                  Congratulations! your account is successfully activated,
                  <br />
                  Please close this tab and login to app with the password
                  attached to the activation email
                </Typography>
              ) : (
                <React.Fragment>
                  <Typography variant="h5" className={classes.h5}>
                    {client} is feeling more that happy to welcome you to their
                    family
                  </Typography>
                  <Typography variant="h5" className={classes.h5}>
                    Don't worry about your On-Boarding, Liteboards is here to
                    take care of everything
                  </Typography>
                  <Typography variant="h5" className={classes.h5}>
                    Please click on the <strong>Activate</strong> button below
                    to initiate your On-Boarding
                  </Typography>
                </React.Fragment>
              )}
            </CardContent>
            {successMessage ? (
              ""
            ) : (
              <CardActions className={classes.cardAction}>
                <Button
                  size="large"
                  variant="contained"
                  className={classes.button}
                  onClick={() => activateUser()}
                >
                  <Typography variant="button">Activate</Typography>
                </Button>
              </CardActions>
            )}
          </Card>
        </Box>
      ) : (
        props.history.push("/")
      )}
    </React.Fragment>
  );
}
