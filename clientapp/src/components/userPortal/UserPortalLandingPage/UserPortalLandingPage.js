// REACT IMPORTS ----------------------------------------------------------------------------------
import React from "react";

// UI IMPORTS ----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
// COMPONENT IMPORTS ----------------------------------------------------------------------------------
import Calender from "../../shared/Calender";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  calendar: {
    width: "80%",
    [theme.breakpoints.down("md")]: {
      width: "100%",
      height: "70%",
    },
    height: "60%",
    boxShadow: "20px 15px 50px #d6d6d6",
    border: "0px",
    fontSize: "20px",
  },
  tile: {
    fontSize: "20px",
    height: "60px",
  },
}));

export default function UserPortalLandingPage() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container style={{ height: "800px" }}>
        <Grid item xl={6} l={6} md={6} xs={12} sm={12}></Grid>
        <Grid item xl={6} l={6} md={6} xs={12} sm={12}>
          <div className={classes.root}>
            <Calender />
          </div>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
