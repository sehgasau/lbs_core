// REACT IMPORTS ----------------------------------------------------------------------------------
import React, { useState, useEffect, useRef } from "react";
import ioClient from "socket.io-client";
import { connect } from "twilio-video";
import { getVideoConfig, stopStream } from "../../../util/videoConfig";
// UI IMPORTS ----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
// COMPONENT IMPORTS ----------------------------------------------------------------------------------
import SplashPage from "./splashPage";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexGrow: 1,
    height: "100vh",
    backgroundColor: "#242424",
  },
  topGridBar: {
    borderBottom: "3px inset #08f",
    height: "20%",
    width: "100%",
    padding: "10px",
    marginBottom: "1%",
    animation: `$lightBlink 2000ms infinite`,
  },
  mainViewGrid: {
    height: "77%",
    backgroundColor: "#242424",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  chatGrid: {
    height: "77%",
    backgroundColor: "#242424",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  topGridVideo: {
    height: "90%",
    width: "15%",
    [theme.breakpoints.down("md")]: {
      width: "40%",
    },
    backgroundColor: "#383838",
    marginRight: "1%",
    marginTop: "0.5%",
  },
  topGridDiv: {
    width: "100%",
    height: "100%",
    overflowX: "scroll",
    overflowY: "hidden",
    display: "flex",
  },
  mainVideo: {
    width: "98%",
    height: "95%",
  },
  chatDiv: {
    width: "90%",
    height: "95%",
    backgroundColor: "#383838",
  },
  "@keyframes lightBlink": {
    "0%": {
      boxShadow: "0 0 0px #319bf7",
    },
    "10%": {
      boxShadow: "0 0 5px #319bf7",
    },
    "20%": {
      boxShadow: "0 0 10px #319bf7",
    },
    "30%": {
      boxShadow: "0 0 15px #319bf7",
    },
    "40%": {
      boxShadow: "0 0 20px #319bf7",
    },
    "50%": {
      boxShadow: "0 0 30px #319bf7",
    },
    "60%": {
      boxShadow: "0 0 20px #319bf7",
    },
    "70%": {
      boxShadow: "0 0 15px #319bf7",
    },
    "80%": {
      boxShadow: "0 0 10px #319bf7",
    },
    "90%": {
      boxShadow: "0 0 5px #319bf7",
    },
    "100%": {
      boxShadow: "0 0 0px #319bf7",
    },
  },
}));

const EventRoom = (props) => {
  const classes = useStyles();
  const [participants, setParticipants] = useState(["1", "2", "1"]);
  const [selfParticipant, setSelfParticipant] = useState(null);
  const [selfStream, setSelfStream] = useState(null);
  const videoElSelf = useRef(null);
  const [devices, setDevices] = useState({
    audio: [],
    video: [],
  });
  const [selectedDevices, setSelectedDevices] = useState({
    audio: null,
    video: null,
  });
  const handleDeviceChange = (device, e) => {
    setSelectedDevices((selectedDevices) => ({
      ...selectedDevices,
      [device]: e.target.value,
    }));
  };

  const initVideoConfig = async () => {
    try {
      const { devices } = await getVideoConfig();
      setDevices(devices);
      setSelectedDevices({
        audio: devices.audio[0],
        video: devices.video[0],
      });
      setSelfStream(window.stream);
    } catch (err) {
      console.log(err);
    }
  };

  const setParticipantsUi = () => {
    return participants.map((participant) => {
      return <video className={classes.topGridVideo}></video>;
    });
  };
  useEffect(() => {
    initVideoConfig();
    if (videoElSelf.current) {
      videoElSelf.current.srcObject = window.stream;
    }
  }, [selfParticipant]);

  return (
    <React.Fragment>
      {!selfParticipant ? (
        <SplashPage
          selectedDevices={selectedDevices}
          devices={devices}
          handleDeviceChange={handleDeviceChange}
          toggleRoom={() => {
            setSelfParticipant("self");
          }}
        />
      ) : (
        <Grid container fluid className={classes.root}>
          <Grid
            item
            xs={12}
            sm={12}
            md={12}
            l={12}
            xl={12}
            className={classes.topGridBar}
          >
            <div className={classes.topGridDiv}>{setParticipantsUi()}</div>
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={9}
            l={9}
            xl={9}
            className={classes.mainViewGrid}
          >
            <video
              autoPlay
              muted
              ref={videoElSelf}
              className={classes.mainVideo}
            ></video>
            <Button
              onClick={() => {
                //stopStream()
                setSelfParticipant(null);
              }}
              variant="contained"
            >
              Leave Room
            </Button>
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={3}
            l={3}
            xl={3}
            className={classes.chatGrid}
          >
            <div className={classes.chatDiv}></div>
          </Grid>
        </Grid>
      )}
    </React.Fragment>
  );
};

export default EventRoom;
