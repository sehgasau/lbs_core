// REACT IMPORTS ----------------------------------------------------------------------------------
import React, { useState, useEffect, useRef } from "react";
import ioClient from "socket.io-client";
// UI IMPORTS ----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
// COMPONENT IMPORTS ----------------------------------------------------------------------------------

const SupWelcomePage = (props) => {
  return (
    <React.Fragment>
      <Typography variant="h1">WELCOME TO SUP </Typography>
    </React.Fragment>
  );
};

export default SupWelcomePage;
