// REACT IMPORTS ----------------------------------------------------------------------------------
import React, { useState, useEffect, useRef } from "react";
import ioClient from "socket.io-client";
import { Link } from "react-router-dom";
import * as TwilioService from "../../../util/twilioService";

// UI IMPORTS ----------------------------------------------------------------------------------
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
// COMPONENT IMPORTS ----------------------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexGrow: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
  },
  heading: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-end",
  },
  supLogo: {
    color: "#6766A5",
    marginLeft: "1%",
    marginRight: "1%",
    fontSize: "80px",
  },
  joinButton: {
    backgroundColor: "#6766A5",
    color: "white",
    width: "18%",
    marginTop: "1%",
    height: "50px",
  },
  videoCanvas: {
    width: "100%",
    height: "300px",
    margin: "3%",
  },
  videoConfigDiv: {
    display: "flex",
    flexDirection: "column",
    height: "60%",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  formControl: {
    width: "14%",
    marginRight: "1%",
  },
}));

const SupSplashPage = (props) => {
  const classes = useStyles();
  const videoEl = useRef(null);

  const [self, setSelf] = useState();
  const [room, setRoom] = useState();
  const [participants, setParticipants] = useState();

  useEffect(() => {
    videoEl.current.srcObject = window.stream;
    TwilioService.init("saurav", "someRoom", setSelf, setParticipants, setRoom);
  }, [window.stream, self, room, participants]);

  return (
    <React.Fragment>
      <div className={classes.root}>
        <Typography variant="h2" className={classes.heading}>
          <span style={{ marginBottom: "0.6%", color: "#666666" }}>
            Welcome to
          </span>
          <span className={classes.supLogo}>sÜp</span>
          <span style={{ marginBottom: "0.6%", color: "#7261c2" }}>
            by Liteboards
          </span>
        </Typography>
        <div className={classes.videoConfigDiv}>
          <video
            muted
            autoPlay
            className={classes.videoCanvas}
            ref={videoEl}
          ></video>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <FormControl
              className={classes.formControl}
              variant="filled"
              disabled={false}
            >
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={props.selectedDevices.video}
                onChange={(e) => {
                  props.handleDeviceChange("video", e);
                }}
              >
                {props.devices.video.map((vDevice) => {
                  return <MenuItem value={vDevice}>{vDevice.label}</MenuItem>;
                })}
              </Select>
            </FormControl>
            <FormControl
              className={classes.formControl}
              variant="filled"
              disabled={false}
            >
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={props.selectedDevices.audio}
                onChange={(e) => {
                  props.handleDeviceChange("audio", e);
                }}
              >
                {props.devices.audio.map((aDevice) => {
                  return <MenuItem value={aDevice}>{aDevice.label}</MenuItem>;
                })}
              </Select>
            </FormControl>
          </div>
        </div>
        <Button
          disabled={window.stream ? false : true}
          variant="contained"
          className={classes.joinButton}
          onClick={() => {
            props.toggleRoom();
          }}
        >
          <Typography variant="button">Join</Typography>
        </Button>
      </div>
    </React.Fragment>
  );
};

export default SupSplashPage;
