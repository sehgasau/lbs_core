import Peer from "peer";
import ioClient from "socket.io-client";

const initiateHandshake = (roomId, stream) => {
  const socket = ioClient(`${process.env.EVENT_API_URL}`);
  socket.on("connect", () => {
    const me = initPeer();
    me.on("open", (id) => {
      socket.emit("wanna_join_room", { roomId, peerId: id });
    });
    socket.on("buddy_joined_room", (whoId, connectedRoomId) => {
      // verify the roomId is correct
      if (roomId === connectedRoomId) connectToPeer(me, whoId, stream);
      else console.log("buddy this is wrong room");
    });
  });
};

// PEER -------------------------------------------------------------------------------

const initPeer = (url) => {
  return (peer = new Peer(undefined, {
    path: `${process.env.REACT_APP_API}${url}`,
    host: "/",
    port: "3000",
  }));
};

// After recieving call from peer in room, connect to that peer 🤝
const connectToPeer = (me, callTo, stream) => {
  const call = me.call(callTo, stream);
  call.on("stream", (buddyStream) => {
    //something to append the stream to participants array
  });
};

// Broadcast that you joined the room
const makeSomeNoiseInRoom = (me) => {};
