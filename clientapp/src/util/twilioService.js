import {
  connect,
  createLocalTracks,
  createLocalVideoTrack,
} from "twilio-video";
import * as axios from "axios";

// call from useEffect  (useEffect will render according to room, self, participants)
const listenForRoomUpdates = (room, setSelf, setParticipants, setRoom) => {
  const roomMembersInfo = getConnectedMembersInfo(room);
  setParticipants(roomMembersInfo ? roomMembersInfo : null);

  // events
  handleNewParticipant(room, setParticipants);
  eventSelfDisconnect(room, setRoom, setSelf, setParticipants);
};

const initiateConnection = (
  identity,
  roomToJoin,
  localTracks,
  setSelf,
  setRoom
) => {
  axios
    .get(
      `${process.env.REACT_APP_API}/api/v1/event/generateAccessToken/${identity}/${roomToJoin}`
    )
    .then((res) => {
      const { token } = res.data;
      connect(token, { name: roomToJoin, tracks: localTracks }).then(
        (room) => {
          setRoom(room);
          setSelf(room.localParticipant);
        },
        (error) => {
          console.error(`Unable to connect to Room: ${error.message}`);
        }
      );
    })
    .catch((err) => {
      console.log(err);
    });
};

// call only once outside useEffect
const init = (identity, room, setSelf, setParticipants, setRoom) => {
  const trackOptions = {
    audio: true,
    video: { width: 700 },
  };
  createLocalTracks(trackOptions).then(async (localTracks) => {
    console.log(await localTracks[0]._trackSender._track);
    // initiateConnection(identity, room, localTracks,setSelf,setParticipants,setRoom);
  });
};

const handleNewParticipant = (room, setParticipants) => {
  room.on("participantConnected", (participant) => {
    //local participant will listen to this "participantConnected event and will take its tracks and will append em into its ui"
    setParticipants((participants) => [...participants, participant]);
  });

  room.on("participantDisconnected", (participant) => {
    console.log(`Participant disconnected: ${participant.identity}`);
    const updatedParticipants = (participants) => {
      // to test this if it will work or not
      //const index = participants.indexOf(participant)
      //participants.splice(index,1)
      //return participants

      //to try this aswell (more efficient => this will only loop till it finds the element [worst case Z<=n])
      const index = participants.findIndex(
        (p) => p.identity === participant.identity
      );
      participants.splice(index, 1);
      return participants;

      //this will loop through all the elements and will omit out the matched conditiion [worst case Z=n]
      // return participants.filter(p=>{
      //   return p.identity !== participant.identity
      // })
    };
    setParticipants([...updatedParticipants()]);
  });
};

const getConnectedMembersInfo = (room) => {
  let roomMembersInfo = [];
  if (room && room.participants && room.participants.length > 1) {
    room.participants.forEach((participant) => {
      participant.tracks.forEach((publication) => {
        if (publication && publication.track) {
          roomMembersInfo.push({
            participant: participant,
            track: publication.track,
          });
          return;
        }
      });

      participant.on("trackSubscribed", (track) => {
        roomMembersInfo.push({
          participant: participant,
          track: track,
        });
        return;
      });
    });
  }

  return roomMembersInfo;
};

const eventSelfDisconnect = (room, setRoom, setSelf, setParticipants) => {
  room.on("disconnected", (room) => {
    // Detach the local media elements
    room.localParticipant.tracks.forEach((publication) => {
      const attachedElements = publication.track.detach();
      attachedElements.forEach((element) => element.remove());
    });
    setSelf(null);
    setParticipants(null);
  });
  setRoom(null);
};

const getCameraPreview = async () => {
  return await createLocalVideoTrack();
};

const disconnectFromRoom = (room) => {
  room.disconnect();
};

export { init, getCameraPreview, disconnectFromRoom };
