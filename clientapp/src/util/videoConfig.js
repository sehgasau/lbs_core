export const getVideoConfig = () => {
  let devices = {
    audio: [],
    video: [],
  };
  return navigator.mediaDevices
    .enumerateDevices()
    .then(async (deviceInfos) => {
      deviceInfos.map((device) => {
        if (device.kind === "videoinput") {
          devices = {
            ...devices,
            video: [...devices.video, device],
          };
        }
        if (device.kind === "audioinput") {
          devices = {
            ...devices,
            audio: [...devices.audio, device],
          };
        }
      });
      const constraints = {
        audio: true,
        video: true,
      };
      const stream = await getStream(constraints);
      window.stream = stream;
      return {
        devices,
      };
    })
    .catch((err) => {
      console.log(err);
      return null;
    });
};

const getStream = (deviceConstraints) => {
  return navigator.mediaDevices.getUserMedia(deviceConstraints);
};

export const stopStream = () => {
  window.stream.getTracks().forEach(function (track) {
    track.stop();
  });
};
