import * as axios from "axios";
import store from "../global/store/store";

const _get = (url, options, requireAuth, optionalAuthHeader) => {
  const header = {
    type: "GET",
  };
  return axios.get(`${process.env.REACT_APP_API}${url}`, {
    params: options ? { options } : null,
    headers:
      requireAuth === true
        ? {
            Authorization: `Bearer ${
              !optionalAuthHeader
                ? store.getState().userInfo.token
                : optionalAuthHeader.token
            }`,
            role: !optionalAuthHeader
              ? store.getState().userInfo.role
              : optionalAuthHeader.role
              ? optionalAuthHeader.role
              : "ACTION_REQ",
          }
        : header,
  });
};
const _post = (url, payload, requireAuth, optionalAuthHeader) => {
  const header = {
    type: "POST",
  };
  return axios.post(
    `${process.env.REACT_APP_API}${url}`,
    payload ? payload : null,
    {
      headers:
        requireAuth === true
          ? {
              Authorization: `Bearer ${
                !optionalAuthHeader
                  ? store.getState().userInfo.token
                  : optionalAuthHeader.token
              }`,
              role: !optionalAuthHeader
                ? store.getState().userInfo.role
                : optionalAuthHeader.role
                ? optionalAuthHeader.role
                : "ACTION_REQ",
            }
          : header,
    }
  );
};
const _put = (url, payload, requireAuth, optionalAuthHeader) => {
  const header = {
    type: "PUT",
  };
  return axios.put(
    `${process.env.REACT_APP_API}${url}`,
    payload ? payload : null,
    {
      headers:
        requireAuth === true
          ? {
              Authorization: `Bearer ${
                !optionalAuthHeader
                  ? store.getState().userInfo.token
                  : optionalAuthHeader.token
              }`,
              role: !optionalAuthHeader
                ? store.getState().userInfo.role
                : optionalAuthHeader.role
                ? optionalAuthHeader.role
                : "ACTION_REQ",
            }
          : header,
    }
  );
};
const _delete = () => {};

const Axios = {
  _get,
  _post,
  _put,
  _delete,
};
export default Axios;
