import actionTypes from './actionTypes';

const _startGlobalLoading = ()=>{
    return {
        type: actionTypes.GLOBALLOADINGSTART,
        payload:true,
    }
}
const _stopGlobalLoading = () =>{
    return{
        type: actionTypes.GLOBALLOADINGSTOP,
        payload:false,
    }
}

const _setDarkTheme = () =>{
    return{
        type: actionTypes.SETDARKTHEME,
        payload:'dark',
    }
}

export default {
    _startGlobalLoading,
    _stopGlobalLoading,
    _setDarkTheme
}

