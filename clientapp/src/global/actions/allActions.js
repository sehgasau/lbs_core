import userActions from '../actions/userActions';
import globalPropertiesActions from '../actions/globalPropertiesActions'

export default {
    userActions,
    globalPropertiesActions
}