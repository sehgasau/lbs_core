import actionTypes from './actionTypes';

const _logout = ()=>{
    return {
        type: actionTypes.LOGOUT,
        payload:{},
    }
}

const _saveUserToState = (payload) =>{
    return {
        type: actionTypes.AUTH,
        payload:payload,
    }
}


export default {
    _logout,
    _saveUserToState
}

