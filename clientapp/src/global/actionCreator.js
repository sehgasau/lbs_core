import actions from "../global/actions/allActions";
import Axios from "../config/axiosConfig";
import jwtDecode from "jwt-decode";

// AUTHENTICATE A USER
const authenticate = (payload) => {
  return function (dispatch) {
    return Axios._post(`/user/authenticate`, payload, false)
      .then((response) => {
        if (response.status === 200) {
          const {
            data: { token, role, isActivated },
          } = response;
          console.log(isActivated);
          if (!isActivated) {
            throw "Please activate your account first";
          }
          loadUserInfo({ token }, dispatch);
          return "success";
        }
      })
      .catch((err) => {
        if (err === "Please activate your account first") {
          return err;
        }
        return null;
      });
  };
};
const logout = (role) => {
  return function (dispatch) {
    clearUserTokenFromLS(role);
    dispatch(actions.userActions._logout());
  };
};

const updateUserState = (payload) => {
  return function (dispatch) {
    dispatch(actions.userActions._saveUserToState(payload));
  };
};

// UPDATE USER STATE AND ADD TOKEN TO LS
const loadUserInfo = ({ token }, dispatch) => {
  const decodedToken = jwtDecode(token);
  const { userId, exp } = decodedToken;

  Axios._get(`/user/single-userAndClient/${userId}`, null, true, { token })
    .then((res) => {
      const { user } = res.data;
      let role = user.role;
      let payload = {
        userId: user.userId,
        email: user.email,
        role: user.role,
        client: user.client,
        token: token,
        isActivated: user.isActivated,
      };
      if (!payload.isActivated) {
        throw "Please activate your account first";
      }
      if (role === "SUPER_ADMIN") {
        loadClientNames(token, role).then((res) => {
          const clients = res.data;
          payload = { ...payload, clients };
          dispatch(actions.userActions._saveUserToState(payload));
        });
      } else {
        dispatch(actions.userActions._saveUserToState(payload));
      }
      const adminPattern = new RegExp(".ADMIN$");
      if (adminPattern.test(role)) {
        role = "ADMIN";
      }

      saveUserTokenToLS({ token, exp, role });
    })
    .catch((err) => {
      dispatch(actions.userActions._logout());
    });
};

const saveUserTokenToLS = (payload) => {
  const { token, exp, role } = payload;
  localStorage.setItem(`liteboardscom_${role}token`, token);
  localStorage.setItem(`liteboardscom_${role}exp`, exp);
};

const clearUserTokenFromLS = (role) => {
  localStorage.removeItem(`liteboardscom_${role}token`);
  localStorage.removeItem(`liteboardscom_${role}exp`);
};

// here role is temp
const tryAutoAuthentication = (role) => {
  return function (dispatch) {
    const token = localStorage.getItem(`liteboardscom_${role}token`);
    const exp = localStorage.getItem(`liteboardscom_${role}exp`);
    // const date = new Date();
    const now = new Date().getTime() / 1000;

    if (token && exp - now > 0) {
      console.log("loading user....");
      loadUserInfo({ token }, dispatch);
    } else {
      dispatch(logout());
    }
  };
};

const loadClientNames = (token, role) => {
  return Axios._get(`/client/clients`, null, true, { token, role });
};

export default {
  authenticate,
  logout,
  tryAutoAuthentication,
  updateUserState,
};
