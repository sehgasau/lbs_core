import allReducers from '../reducers/allReducers.js';
const redux = require('redux')
const compose = redux.compose
const createStore = redux.createStore
const applyMiddleware = redux.applyMiddleware;
//import thunkMiddleware from 'redux-thunk';
const thunkMiddleware = require('redux-thunk').default

const store = createStore(allReducers, compose(applyMiddleware(thunkMiddleware)));
export default store;
//, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()