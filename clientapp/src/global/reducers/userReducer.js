import axios from 'axios';
import jwtDecode from 'jwt-decode';
import redux from 'redux';
import applyMiddleware from 'redux-thunk';
import * as thunkMiddleware from 'redux-thunk';
import userActions from '../actions/userActions';

const initialState = null;

const userReducer = (state = initialState, {type, payload} = userActions)=>{
    switch(type){
        case 'AUTH' : {
            return payload
        }
        case 'LOGOUT': return null;
        default : return state;
    }
}

export default userReducer;