import {combineReducers} from 'redux';
import userReducer from './userReducer';
import globalPropertiesReducer from './globalPropertiesReducer';

const allReducers = combineReducers({
    userInfo:userReducer,
    globalProperties:globalPropertiesReducer,
});

export default allReducers;
