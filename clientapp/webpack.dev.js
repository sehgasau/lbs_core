var webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

// everything should be in sync with public path    like all the static routes in serverConfig and react router routes as well

module.exports = {
  entry: ["babel-polyfill", "./clientapp/src/index.js"],
  mode: "production",

  output: {
    path: path.join(__dirname, "/dist"),
    filename: "bundelDev.[hash].js",
    publicPath: "/",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["react", "stage-0"],
          plugins: [
            "transform-class-properties",
            "transform-object-rest-spread",
            [
              "babel-plugin-import",
              {
                libraryName: "@material-ui/core",
                // Use "'libraryDirectory': ''," if your bundler does not support ES modules
                libraryDirectory: "",
                camel2DashComponentName: false,
              },
              "core",
            ],
            [
              "babel-plugin-import",
              {
                libraryName: "@material-ui/icons",
                // Use "'libraryDirectory': ''," if your bundler does not support ES modules
                libraryDirectory: "",
                camel2DashComponentName: false,
              },
              "icons",
            ],
          ],
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/,
        use: ["url-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./clientapp/public/index.html",
    }),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": '"development"',
      "process.env.REACT_APP_API": '"/api/v1"',
    }),
  ],
};
