import { PrimaryGeneratedColumn, Column, CreateDateColumn } from "typeorm";

export default class LoggerAbstract {
  @PrimaryGeneratedColumn("uuid")
  logId: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  type: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  env: string;

  @CreateDateColumn()
  loggingDate: Date;

  @CreateDateColumn()
  logLatestUpdateDate: Date;
}
