import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from "typeorm";
import LoggerAbstract from "./LoggerAbstract";
import * as CONFIG from "../../config/config.mapper";

@Entity({ name: "logServiceContext", database: CONFIG["LBCONFIG_LOG_DB"] })
export default class LogServiceContext extends LoggerAbstract {
  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  message: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  context: string;
}
