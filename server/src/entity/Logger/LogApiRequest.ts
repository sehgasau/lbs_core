import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
  } from "typeorm";
  import LoggerAbstract from "./LoggerAbstract";
  import * as CONFIG from "../../config/config.mapper";

  @Entity({ name: "logApiRequests", database: CONFIG["LBCONFIG_LOG_DB"] })
  export default class LogApiRequests extends LoggerAbstract {
    @Column({
      type: "varchar",
      length: 150,
      unique: false,
      nullable: true,
    })
    message: string;
  
    @Column({
      type: "varchar",
      length: 150,
      unique: false,
      nullable: false,
    })
    context: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false,
      })
      routePath: string;

      @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false,
      })
      reqMethod: string;

      @Column({
        unique: false,
        nullable: false,
        default:false
      })
      isProtectedRoute: boolean;
  }
  