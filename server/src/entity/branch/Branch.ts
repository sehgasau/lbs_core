import {Entity, PrimaryGeneratedColumn,OneToMany,Column, ManyToOne, JoinColumn, CreateDateColumn} from "typeorm";
import User from '../user/User';
import Client from '../client/Client';

@Entity({name:'branch'})
export default class Branch{

    @PrimaryGeneratedColumn("uuid")
    branchId: string;

    @OneToMany(type => User, user => user.client)
    users: User[];

    @Column()
    clientId:string
    @ManyToOne(type => Client, client => client.branches)
    @JoinColumn({name:'clientId'})
    client: Client;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: true
    })
    name: string;

    @Column({
        type: "varchar",
        length: 255,
        unique: true,
        nullable: false
    })
    address: string;

    @Column({
        type: "varchar",
        length: 255,
        unique: false,
        nullable: false
    })
    role: string;

    @CreateDateColumn()
    creationDate: Date;

    @CreateDateColumn()
    latestUpdateDate: Date;
}
