import { CreateDateColumn, UpdateDateColumn, Column } from "typeorm";

export default abstract class DateColumns {
  @CreateDateColumn()
  creationDate: Date;

  @UpdateDateColumn()
  latestUpdateDate: Date;

  @Column({
    unique: false,
    nullable: true,
  })
  removalDate: Date;
}
