import { Column, CreateDateColumn, UpdateDateColumn } from "typeorm";

export default abstract class Content {
  @Column({
    unique: false,
    nullable: false,
    default: false,
  })
  isActivated: boolean;

  @Column({
    type: "varchar",
    length: 20,
    unique: false,
    nullable: false,
  })
  role: string;

  @Column({
    type: "datetime",
    unique: false,
    nullable: true,
  })
  activationDate: Date;

  @Column({
    type: "datetime",
    unique: false,
    nullable: true,
  })
  deactivationDate: Date;

  @CreateDateColumn()
  creationDate: Date;

  @UpdateDateColumn()
  latestUpdateDate: Date;
}
