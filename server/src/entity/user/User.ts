import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  JoinColumn,
  OneToMany,
} from "typeorm";
import AbstractColumns from "../abstract/AbstractColumns";
import Client from "../client/Client";
import Group from "../group/Group";
import {
  Contains,
  IsInt,
  Length,
  IsEmail,
  IsFQDN,
  IsDate,
  Min,
  Max,
  ValidateIf,
} from "class-validator";
import Branch from "../branch/Branch";
import EventRoom from "../eventRoom/EventRoom";

@Entity({ name: "user" })
export default class User extends AbstractColumns {
  @PrimaryGeneratedColumn("uuid")
  userId: string;

  @OneToMany((type) => EventRoom, (event) => event.user)
  events: EventRoom[];

  @Column()
  clientId: string;
  @ManyToOne((type) => Client, (client) => client.users)
  @JoinColumn({ name: "clientId" })
  client: Client;

  @ManyToOne((type) => Group, (group) => group.users)
  group: Group;

  @Column({
    nullable: true,
  })
  branchId: string;
  @ManyToOne((type) => Branch, (branch) => branch.users)
  @JoinColumn({ name: "branchId" })
  branch: Branch;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  name: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: true,
  })
  preferredName: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  designation: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: true,
    nullable: true,
  })
  @ValidateIf((o) => o.personalEmail !== undefined)
  @IsEmail()
  personalEmail: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: true,
    nullable: true,
  })
  @IsEmail()
  workEmail: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  password: string;

  @Column({
    type: "varchar",
    length: 255,
    unique: false,
    nullable: true,
  })
  avatar: string;

  @Column({
    type: "varchar",
    length: 255,
    unique: false,
    nullable: true,
  })
  address: string;

  @Column({
    type: "varchar",
    length: 255,
    unique: true,
    nullable: true,
  })
  phone: string;

  @Column({
    type: "varchar",
    length: 255,
    unique: false,
    nullable: true,
  })
  dob: string;

  @Column({
    unique: false,
    nullable: false,
    default: false,
  })
  isLoggedIn: boolean;
}
