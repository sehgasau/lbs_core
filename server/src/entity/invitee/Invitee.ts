import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable } from "typeorm";
import DateColumns from "../abstract/DateColumns";
import EventRoom from "../eventRoom/EventRoom";

export enum GoingStatus {
  YES = "yes",
  NO = "no",
  NOT_SURE = "not sure",
}

export enum JoinStatus {
  JOINED = "joined",
  WAITING = "waiting",
  INACTIVE = "in-active",
}

@Entity({ name: "invitee" })
export class Invitee extends DateColumns { 
  @PrimaryGeneratedColumn("uuid")
  inviteeId: string;

  @Column({
    unique: false,
    nullable: true,
  })
  userId: string;

  @ManyToMany((type) => EventRoom,  (eventRoom) => eventRoom.invitees)
  eventRooms: EventRoom[];

  @Column({
    unique: false,
    nullable: false,
    default: false,
  })
  isHost: boolean;

  @Column({
    unique: false,
    nullable: false,
    default: true,
  })
  isInvitee: boolean;

  @Column({
    type: "varchar",
    length: 255,
    unique: false,
    nullable: true,
  })
  messageToInvitee: string;

  @Column({
    unique: false,
    nullable: true,
  })
  dateInvited: Date;

  @Column({
    unique: false,
    nullable: true,
  })
  goingStatus: GoingStatus;

  @Column({
    unique: false,
    nullable: true,
  })
  isExternalInvitee: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  hasLeftRoom: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  isMuted: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  isViewable: boolean;

  @Column({
    type: "varchar",
    length: 50,
    unique: true,
    nullable: true,
  })
  email: string;

  @Column({
    unique: false,
    nullable: true,
    default: JoinStatus.INACTIVE,
  })
  joinStatus: JoinStatus;
}
