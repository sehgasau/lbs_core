import {Entity, PrimaryGeneratedColumn,ManyToOne,Column, CreateDateColumn, UpdateDateColumn, OneToMany} from "typeorm";
import AbstractColumns from '../abstract/AbstractColumns';
import Client from '../client/Client';
import User from '../user/User';
@Entity({name:'group'})
export default class Group {

    @PrimaryGeneratedColumn("uuid")
    groupId: string;

    @ManyToOne(type => Client, client => client.groups)
    client: Client;

    @OneToMany(type => User, user => user.group)
    users: User[];

    @Column({
        type: "varchar",
        length: 150,
        nullable: true
    })
    name: string;

    @CreateDateColumn()
    creationDate: Date;

    @UpdateDateColumn()
    latestUpdateDate: Date;

}
