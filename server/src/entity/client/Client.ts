import {Entity, PrimaryGeneratedColumn,OneToMany,Column, OneToOne} from "typeorm";
import AbstractColumns from '../abstract/AbstractColumns';
import User from '../user/User';
import Group from '../group/Group';
import JobPosting from "../job/JobPosting";
import Branch from "../branch/Branch";
import WhiteLabelConfig from "./WhiteLabelConfig";

@Entity({name:'client'})
export default class Client extends AbstractColumns{

    @PrimaryGeneratedColumn("uuid")
    clientId: string;

    @OneToOne(type => WhiteLabelConfig, wlConfig => wlConfig.client) // specify inverse side as a second parameter
    wlConfig: WhiteLabelConfig;

    @OneToMany(type => User, user => user.client)
    users: User[];

    @OneToMany(type => Group, group => group.client)
    groups: Group[];

    @OneToMany(type => Branch, branch => branch.client)
    branches: Branch[];

    @OneToMany(type => JobPosting, jobPosting => jobPosting.client)
    jobPostings: JobPosting[];

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
        nullable: false
    })
    name: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false
    })
    industryType: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false
    })
    subscriptionType: string;
}
