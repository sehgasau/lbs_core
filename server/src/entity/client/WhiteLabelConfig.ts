import {Entity, PrimaryGeneratedColumn,OneToOne,Column, ManyToOne, JoinColumn, CreateDateColumn} from "typeorm";
import DateColumns from "../abstract/DateColumns";
import Client from './Client';

@Entity({name:'whiteLabelConfig'})
export default class WhiteLabelConfig extends DateColumns{

    @PrimaryGeneratedColumn("uuid")
    branchId: string;

    @OneToOne(type => Client, client => client.wlConfig) // specify inverse side as a second parameter
    @JoinColumn({name:'clientId'})
    client: Client;

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
        nullable: true
    })
    url: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
        nullable: true
    })
    logoUrl: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: true
    })
    primaryColor: string;

    // will contain the custom theme in JSON object - fixed schema
    @Column({
        type: "varchar",
        length: 255,
        unique: false,
        nullable: true
    })
    themeSchema: string;

    // will contain the custom terms and policies, texts, descriptions and other client specific settings in JSON object - fixed schema
    @Column({
        type: "varchar",
        length: 255,
        unique: false,
        nullable: true
    })
    appSettingSchema: string;

    @Column({
        unique: false,
        nullable: false,
        default:false
    })
    isConfigActive: boolean;

}
