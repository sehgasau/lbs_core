import {
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  Column,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  Timestamp,
  ManyToMany,
  JoinTable,
} from "typeorm";
import {IsString, IsDate, IsBoolean, IsNumber,ValidateIf} from "class-validator";
import User from "../user/User";
import Client from "../client/Client";
import DateColumns from "../abstract/DateColumns";
import { Invitee } from "../invitee/Invitee";

@Entity({ name: "eventroom" })
export default class EventRoom extends DateColumns {
  @PrimaryGeneratedColumn("uuid")
  eventId: string;

  @Column( {nullable: true})
  userId;
  @ManyToOne((type) => User, (user) => user.events)
  @JoinColumn({ name: "userId" })
  user: User;

  @ManyToMany((type) => Invitee, (invitee) => invitee.eventRooms, {
    cascade: ["insert", "update", "remove", "soft-remove", "recover"],
  })
  @JoinTable()
  invitees: Invitee[];

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: false,
  })
  @ValidateIf((o) => o.eventType !== undefined)
  @IsString()
  eventType: string;

  @Column({
    type: "varchar",
    length: 150,
    unique: false,
    nullable: true,
  })
  @ValidateIf((o) => o.eventDescription !== undefined)
  @IsString()
  eventDescription: string;

  @Column({
    type: "varchar",
    length: 255,
    unique: true,
    nullable: true,
  })
  @ValidateIf((o) => o.eventUrl !== undefined)
  @IsString()
  eventUrl: string;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.isClosed !== undefined)
  @IsBoolean()
  isClosed: boolean;

  @Column({
    type: "datetime",
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.eventDate !== undefined)
  @IsDate()
  eventDate: Date;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.isPostponed !== undefined)
  @IsBoolean()
  isPostponed: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.isPaused !== undefined)
  @IsBoolean()
  isPaused: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.isCompleted !== undefined)
  @IsBoolean()
  isCompleted: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.completionDate !== undefined)
  @IsDate()
  completionDate: Date;

  @Column({
    type: "varchar",
    length: 255,
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.location !== undefined)
  @IsString()
  location: string;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.from !== undefined)
  @IsDate()
  from: Date;

  @Column({
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.to !== undefined)
  @IsDate()
  to: Date;

  @Column({
    type: "int",
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.totalInvitees !== undefined)
  @IsNumber()
  totalInvitees: number;

  @Column({
    type: "int",
    unique: false,
    nullable: true,
  })
  @ValidateIf((o)=>o.attendence !== undefined)
  @IsNumber()
  attendence: number;
}
