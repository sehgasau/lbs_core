import {Entity, PrimaryGeneratedColumn,OneToMany,ManyToOne,Column,CreateDateColumn,UpdateDateColumn} from "typeorm";
import Client from '../client/Client';
import JobApplication from './JobApplication';

@Entity({name:'jobposting'})
export default class JobPosting {

    @PrimaryGeneratedColumn("uuid")
    jobId: string;

    @ManyToOne(type => Client, client => client.jobPostings)
    client: Client;

    @OneToMany(type => JobApplication, jobApplication => jobApplication.jobPosting)
    jobApplications: JobApplication[];

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false
    })
    title: string;

    @Column({
        type: "varchar",
        length: 255,
        unique: false,
        nullable: false
    })
    description: string;

    @Column({
        type: "int",
        unique: false,
        nullable: false,
        default:0
    })
    visits: Int16Array;

    @CreateDateColumn()
    postDate: Date;

    @UpdateDateColumn()
    jobPostUpdateDate: Date;
}
