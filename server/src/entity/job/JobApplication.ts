import {Entity, PrimaryGeneratedColumn,OneToMany,ManyToOne,Column,CreateDateColumn,UpdateDateColumn} from "typeorm";
import Client from '../client/Client';
import JobPosting from './JobPosting';
import {Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

@Entity({name:'jobapplication'})
export default class JobApplication {

    @PrimaryGeneratedColumn("uuid")
    jobApplicationId: string;

    @ManyToOne(type => JobPosting, jobPosting => jobPosting.jobApplications)
    jobPosting: JobPosting;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false
    })
    userName: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false
    })
    @IsEmail()
    userEmail: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: false,
        nullable: false
    })
    userResumeUrl: string;

    @Column({
        type: "varchar",
        length: 255,
        unique: false,
        nullable: false
    })
    userDescription: string;

    @Column({
        type: "varchar",
        length: 255,
        unique: false,
        nullable: true
    })
    otherInfo: string;

    @CreateDateColumn()
    applyDate: Date;

}
