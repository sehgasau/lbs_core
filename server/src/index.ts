import "reflect-metadata";
import { createConnection, Connection, ConnectionOptions } from "typeorm";
import { ServerConfig, IServerConfig } from "./ServerConfig";
import config = require("./ConnectionConfig");
const connectionOpts: ConnectionOptions = config;

createConnection(connectionOpts)
  .then(async (connection: Connection) => {
    const server: IServerConfig = new ServerConfig(connection);
    server.createServer();
  })
  .catch((error) => console.log(error));
 