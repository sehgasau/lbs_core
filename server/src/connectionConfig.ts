import { ConnectionOptions } from "typeorm";
import * as path from "path";
const CONFIG = require('./config/config.mapper')
const config: ConnectionOptions = {
  type: "mysql",
  host: CONFIG["LBCONFIG_HOST"],
  port: parseInt(CONFIG["LBCONFIG_DB_PORT"]),
  username: CONFIG["LBCONFIG_DB_USERNAME"],
  password: CONFIG["LBCONFIG_PASSWORD"],
  database: CONFIG["LBCONFIG_DATABASE"],
  synchronize: false,
  logging: false,
  entities: [
    path.join(__dirname, "/entity/**/*.js"),
    path.join(__dirname, "/entity/**/*.js.map"),
    path.join(__dirname, "/entity/**/**/*.js"),
    path.join(__dirname, "/entity/**/**/*.js.map"),
    path.join(__dirname, "/entity/**/*.ts"),
    path.join(__dirname, "/entity/**/**/*.ts"),
  ],
  migrations: [
    path.join(__dirname, "/migration/**/*.ts"),
    path.join(__dirname, "/migration/**/*.js"),
  ],
  subscribers: [path.join(__dirname, "/subscriber/**/*.ts")],
  cli: {
    entitiesDir: path.join(__dirname, "/entity"),
    migrationsDir: "server/src/migration",
    subscribersDir: path.join(__dirname, "/subscriber"),
  },
};

export = config;
