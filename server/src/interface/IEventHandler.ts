import { Subscriber } from "../util/types/EventHandlerTypes";

export default interface IEventHandler<T> {
  subscribers: Array<Subscriber>;
  _raiseEvent: (removeSubscribers: boolean, eventArgs?: T) => void;
  _removeAllSubscribers: () => void;
}
