export interface ISmtpTransporter {
  initiateTransporter: () => any;
}

export interface IEmailData {
  to: string;
  from?: string;
  html: string;
  subject: string;
  attachments?: Array<object>;
}
