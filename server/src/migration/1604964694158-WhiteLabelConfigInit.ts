import {MigrationInterface, QueryRunner} from "typeorm";

export class WhiteLabelConfigInit1604964694158 implements MigrationInterface {
    name = 'WhiteLabelConfigInit1604964694158'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `whiteLabelConfig` (`creationDate` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `latestUpdateDate` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `removalDate` datetime NULL, `branchId` varchar(36) NOT NULL, `url` varchar(150) NULL, `logoUrl` varchar(150) NULL, `primaryColor` varchar(150) NULL, `themeSchema` varchar(255) NULL, `appSettingSchema` varchar(255) NULL, `isConfigActive` tinyint NOT NULL DEFAULT 0, `clientId` varchar(36) NULL, UNIQUE INDEX `IDX_b3c85e774bba88a898475aa6c3` (`url`), UNIQUE INDEX `IDX_8d26916555d63f3fe3adb284ec` (`logoUrl`), UNIQUE INDEX `REL_10b8c5ee3a263c38eea7710b2c` (`clientId`), PRIMARY KEY (`branchId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `whiteLabelConfig` ADD CONSTRAINT `FK_10b8c5ee3a263c38eea7710b2ce` FOREIGN KEY (`clientId`) REFERENCES `client`(`clientId`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `whiteLabelConfig` DROP FOREIGN KEY `FK_10b8c5ee3a263c38eea7710b2ce`");
        await queryRunner.query("DROP INDEX `REL_10b8c5ee3a263c38eea7710b2c` ON `whiteLabelConfig`");
        await queryRunner.query("DROP INDEX `IDX_8d26916555d63f3fe3adb284ec` ON `whiteLabelConfig`");
        await queryRunner.query("DROP INDEX `IDX_b3c85e774bba88a898475aa6c3` ON `whiteLabelConfig`");
        await queryRunner.query("DROP TABLE `whiteLabelConfig`");
    }

}
