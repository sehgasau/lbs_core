import * as PasswordGenerator from "generate-password";
import * as bcrypt from "bcrypt";

export default class PasswordUtil {
  private static SALTROUND = 10;

  public static generatePassword(): string {
    const tempPassword: string = PasswordGenerator.generate({
      length: 8,
      numbers: true,
      symbols: true,
      uppercase: true,
      lowercase: true,
    });
    return tempPassword;
  }

  public static generateEncryptedPassword(): Promise<string> {
    const tempPassword: string = PasswordGenerator.generate({
      length: 8,
      numbers: true,
      symbols: true,
      uppercase: true,
      lowercase: true,
    });
    return PasswordUtil.encryptPassword(tempPassword);
  }

  public static encryptPassword(passwordToEncrypt: string): Promise<string> {
    return bcrypt.hash(passwordToEncrypt, PasswordUtil.SALTROUND);
  }

  public static comparePassword(
    passwordToCompare: string,
    actualPassword: string
  ): Promise<boolean> {
    return bcrypt.compare(passwordToCompare, actualPassword);
  }
}
