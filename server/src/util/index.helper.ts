import Types from "./types/index.types";

export class Helper {
  public static initiateAllRoutes = (cbs: Function[]) => {
    if (cbs && cbs.length > 0) {
      cbs.map((cb) => {
        cb();
      });
    }
  };
}
