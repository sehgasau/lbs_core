type EventArg=any
type Subscriber= (eventArgs?:EventArg)=>any;


export {
    EventArg,
    Subscriber
}