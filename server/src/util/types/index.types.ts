import { EventListenerTypes } from "typeorm/metadata/types/EventListenerTypes";
import { Server, Socket } from "socket.io";
import {
  UserEmailService,
  IUserEmailService,
} from "../../services/Notification/UserEmailService";
import { IEventController } from "../../controllers/eventController/EventController";
import { IBranchController } from "../../controllers/branchController/BranchController";
import { IClientController } from "../../controllers/clientController/ClientController";
import { IUserController } from "../../controllers/userController/UserController";
import { Invitee } from "../../entity/invitee/Invitee";

namespace Types {
  export type UserRoles = Array<string>;
  export type CallbackArray = Array<{ route?: string; cb: Function }>;
  export type Array2d<T> = Array<Array<T>>;
  export type MiddlewareCallback = (req,res,next)=>{}
  export type dynamic<T = any> = {
    [key: string]: T;
  };
  export type EventServicePayload = {
    // socket:Socket,
    io: Server;
  };
  export type EventRoomPayload = {
    eventType: string;
    userId: string;
    eventDate: Date;
    location: string;
    from: Date;
    to: Date;
    totalInvitees?: number;
    invitees?: Invitee[];
    eventDescription?: string;
    eventCreatorName?:string;
  };
  export type LoggerType = "HTTP_REQUEST" | "CONTEXT";
}

export default Types;
