export type filterOptions = {
  isActivated?: boolean;
  userName: string;
  cName: string;
  branchName: string;
  offset: number;
  limit: number;
  role: string;
};

export type userInsertPayload = {
  userId: string;
  name: string;
  password: string;
  workEmail: string;
  clientId: string;
  role: string;
  branchId?: string;
};
