import IEventHandler from "../../interface/IEventHandler";
import { EventArg, Subscriber } from "../types/EventHandlerTypes";

export default class EventHandler implements IEventHandler<EventArg> {
  public subscribers: Array<Subscriber>;

  constructor() {
    this.subscribers = new Array<Subscriber>();
  }

  public _raiseEvent(removeSubscribers: boolean, eventArgs?: EventArg) {
    if (eventArgs === undefined) {
      this.subscribers.forEach((sub) => {
        sub();
      });
    } else {
      this.subscribers.forEach((sub) => {
        sub(eventArgs);
      });
    }
    if (removeSubscribers) {
      this._removeAllSubscribers();
    }
  }

  public _removeAllSubscribers() {
    this.subscribers = [];
  }
}
