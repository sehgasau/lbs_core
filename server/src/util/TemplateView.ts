import * as path from 'path';
import * as handlebars from 'handlebars';
import * as fs from 'fs';

export default class TemplateView {

    public static compile = (file:string,replacements)=>{
        const filePath = path.join(file);
        const source = fs.readFileSync(filePath, 'utf-8').toString();
        const templateDelegate = handlebars.compile(source);
        const htmlToSend = templateDelegate(replacements);
        return htmlToSend
    }

}