import { createConnection,createPool } from "mysql";
import Types from "../types/index.types";
import * as CONFIG from "../../config/config.mapper"
export default class MySqlLogTransport {
  private _connection;
  private _pool;
  private _context = "MySqlLogTransport";
  private routeContext = {
    event:"EventRouter",
    client:"ClientRouter",
    user:"UserRouter",
    branch:"BranchRouter"
  }
  constructor(opts) {
    this.initiateMySqlConnection(opts);
  }

  private contextLogger = (info) => {
    const { env, context, message, level: type } = info;
    const insertLog = `insert into logServiceContext(logId,type,message,context,env) 
        values(uuid(),?,?,?,?);
        `;
    const values = [type, message, context, env];
    this._pool.getConnection((err,connection)=>{
      if (err) {
        console.log("ERROR: ",err)
        if(connection)
          connection.release();
        throw err;
      } 
      connection.query(insertLog, [...values], (err, results, fields) => {
        console.log("INSERT RESULT: ", results);
      });
    })
    
  };

  private httpReqLogger = (info) => {
    return (req, res, next) => {
      const {
        message,
        isProtectedRoute,
        level
      } = info;
      const contextKey = req.path.toString().split("/")[1]
      const insertLog = `insert into logApiRequests(logId,type,message,context,env,routePath,isProtectedRoute,reqMethod) 
          values(uuid(),?,?,?,?,?,?,?);
          `;
      const routePath = `${CONFIG["LBCONFIG_SERVER_API_URL"]}${req.baseUrl}${req.path}`;
      const values = [level?level:"info", !message?"":message, this.routeContext[contextKey], CONFIG["LBCONFIG_NODE_ENV"],routePath, isProtectedRoute?isProtectedRoute:false,req.method];
      this._pool.getConnection((err,connection)=>{
        if (err) {
          if(connection)
            connection.release();
          console.log(err)
        } 
        connection.query(insertLog, [...values], (err, results, fields) => {
          if(err)console.log(err)
        });
        connection.release();
      })
      // keep this out of log insertion because log insertion is async and this is delaying the response time
      next();
    };
  };

  public log = (info): void | Types.MiddlewareCallback => {
    const loggerTypeContext = {
      HTTP_REQUEST: this.httpReqLogger,
      CONTEXT: this.contextLogger,
    };
    const logResult = loggerTypeContext[info["loggerType"]](info);
    if(info["loggerType"] === "HTTP_REQUEST")
      return logResult as Types.MiddlewareCallback
  };

  private initiateMySqlConnection = (opts) => {
    const { host, user, password, database } = opts;
    const pool = createPool({
      host: host,
      user: user,
      password: password,
      database: database,
    });

    this._pool = pool;
    this.runTestQuery()

  };

  public runTestQuery = () =>{
    this._pool.getConnection((err,connection)=>{
      if (err) {
        if(connection)
          connection.release();
        throw err;
      } 
      connection.query("select * from logServiceContext", (err, results, fields) => {
        if(err){
          console.log("ERROR from TEST QUERY: ",err) 
        }
        console.log(results)
      });
    })
   
  };

}
