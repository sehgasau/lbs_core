import Types from "../types/index.types";
import MySqlLogTransport from "./MySqlLogTransport";
import * as CONFIG from "../../config/config.mapper";

export default class Logger {
  public static _logger:MySqlLogTransport;

  public static initiateLogger = () => {
    const options = { 
      host: CONFIG["LBCONFIG_HOST"],
      password: CONFIG["LBCONFIG_PASSWORD"],
      user: CONFIG["LBCONFIG_DB_USERNAME"],
      database: CONFIG["LBCONFIG_LOG_DB"],
    };
    Logger._logger = new MySqlLogTransport(options)
  };

  public static log = (info): void | Types.MiddlewareCallback => {
    const loggerReturnVal = Logger._logger.log(info)
    if(info["loggerType"]=="HTTP_REQUEST")
      return loggerReturnVal as Types.MiddlewareCallback

  };
}
 