import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as compression from "compression";
import * as https from "https";
import * as http from "http";
import * as path from "path";
import * as fs from "fs";
import RootRouter from "./controllers/RootRouter";
import RealTimeService, {
  IRealTimeService,
} from "./services/realTime/RealTimeService";
import Logger from "./util/Logger/Logger";
import { Connection, Like } from "typeorm";
import EventService from "./services/realTime/Event/EventService";
import ReportService from "./services/realTime/Reports/ReportService";
import TwilioConfig from "./config/TwilioConfig";
import Types from "./util/types/index.types";
import PasswordUtil from "./util/PasswordUtil/PasswordUtil";
const CONFIG = require("./config/config.mapper")

/**
 * the path in index.html in build should be same as router root path and express root path
 * EXAMPLE:
 * Sup root router path = /welcome
 * Serve dir: /dist/sup
 * WebPack file Public path will be : /welcome
 * index.html build will contain script tag with this path: /welcome/bundel.ads321as.js
 */

export type IServerConfig = InstanceType<typeof ServerConfig>;
export class ServerConfig {
  private _context = "ServerConfig";
  private app;
  private connection: Connection;
  private realTimeService: IRealTimeService;
  private server: https.Server | http.Server;
  constructor(connection: Connection) {
    this.app = express();
    this.initiateConfig([this.activateMiddlewares, Logger.initiateLogger]);
    this.connection = connection;
    this.realTimeService = new RealTimeService();
    this.pushRealtimeServices();
  }

  private initiateConfig = (cbs: Function[]) =>  { 
    cbs.map((cb) => cb());
  };

  private pushRealtimeServices = () => {
    this.realTimeService._eventOnInit.subscribers.push(
      new EventService().init,
      new ReportService().init
    );
  };

  private activateMiddlewares = () => {
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.app.use(cors());
    this.app.use(compression());
  };

  private configRoutes = () => {
    const allRoutes = new RootRouter(this.connection).getRoutes();
    this.app.use("/api/v1",Logger.log({loggerType:"HTTP_REQUEST"}) as Types.MiddlewareCallback,allRoutes);
    if (
      CONFIG["LBCONFIG_NODE_ENV"] === "production" ||
      CONFIG["LBCONFIG_NODE_ENV"] === "development" 
     //|| CONFIG["LBCONFIG_NODE_ENV"] === "local"
    ) {
      this.app.use(
        "/",
        express.static(path.resolve(__dirname, "../../dist/client"))
      );
      this.app.use(
        "*",
        express.static(path.resolve(__dirname, "../../dist/client"))
      );
    } else
      this.app.get("/", (req, res) => {
        res.send("liteboards server active!");
      });
  };

  public createServer = () => {
    this.configRoutes();
    if (CONFIG["LBCONFIG_NODE_ENV"] === "production") {
      this.server = https.createServer(
        {
          key: fs.readFileSync("/root/liteboards.com/ssl/server.key"),
          cert: fs.readFileSync("/root/liteboards.com/ssl/server.crt"),
        },
        this.app
      );
      this.server.listen(CONFIG["LBCONFIG_PORT"], function () {
        console.log(
          `liteboards.com ${CONFIG["LBCONFIG_NODE_ENV"]} : ${CONFIG["LBCONFIG_PORT"]} ts server listening at https://liteboards.com`
        );
      });
    } else {
      this.server = http.createServer(this.app);
      this.server.listen(CONFIG["LBCONFIG_PORT"], () => {
        console.log(
          `liteboards.com ${CONFIG["LBCONFIG_NODE_ENV"]} ts server listening at http://localhost:${CONFIG["LBCONFIG_PORT"]}`
        );
      });
    }
    this.realTimeService.initService(this.server);
  };
}
