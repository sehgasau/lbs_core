import { Socket, listen } from "socket.io";
import Types from "../../util/types/index.types";

import * as https from "https";
import * as http from "http";
import IEventHandler from "../../interface/IEventHandler";
import EventHandler from "../../util/EventDelegation/EventHandler";

export type IRealTimeService = InstanceType<typeof RealTimeService>

export default class RealTimeService {
  public _eventOnInit: IEventHandler<Types.EventServicePayload>;
  private io: SocketIO.Server;
  constructor() {
    this._eventOnInit = new EventHandler();
  }

  public initService = (server: https.Server | http.Server) => {
    this.io = listen(server);
    this.initDefaultServiceIo();
    this.initRoutesServiceIo();
  };

  private initDefaultServiceIo = () => {
    this.io.on("connection", (socket) => {});
  };

  private initRoutesServiceIo = () => {
    this._eventOnInit._raiseEvent(false, { io: this.io });
  };
}
