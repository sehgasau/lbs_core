import { Socket, listen, Server } from "socket.io";
import { Subscriber } from "../../../util/types/EventHandlerTypes";
import Types from "../../../util/types/index.types";
import * as TW from "twilio";
import TwilioConfig from "../../../config/TwilioConfig";
import * as CONFIG from "../../../config/config.mapper";

export type IEventService = InstanceType<typeof EventService>;

export default class EventService {
  private _socket: Socket;
  private SOCKET_ID: string;
  private USER_JOIN_EVENT = CONFIG["LBCONFIG_.USER_JOIN_EVENT"];
  private BROADCAST_EVENT = CONFIG["LBCONFIG_BROADCAST_EVENT"];
  private USER_STREAM_EVENT = CONFIG["LBCONFIG_USER_STREAM_EVENT"];
  private EMIT_STREAM_EVENT = CONFIG["LBCONFIG_EMIT_STREAM_EVENT"];
  private SUCCESS_JOIN_EVENT = CONFIG["LBCONFIG_SUCCESS_JOIN_EVENT"];
  private LEAVE_ROOM_EVENT = CONFIG["LBCONFIG_LEAVE_ROOM_EVENT"];
  private usersInRoom: number;
  constructor() {}

  public init: Subscriber = async (args: Types.EventServicePayload) => {
    TwilioConfig.connectApi();
    // const room = await TwilioConfig.createRoom({
    //   recordParticipantsOnConnect: false,
    //   statusCallback: 'http://example.org',
    //   type: 'group-small',
    //   uniqueName: 'TestRoom3',
    // })
    // console.log(room.status)
    //console.log("ROOM: ",room.uniqueName)
    // await (await TwilioConfig.getAllRooms()).map(room=>{
    //   console.log(room.uniqueName)
    // })
    // const accountSid = process.env.TWILIO_SID
    // const authToken = process.env.TWILIO_AUTH_TOKEN
    // const client = TW(accountSid,authToken)
    // client.video.rooms.create({
    //   recordParticipantsOnConnect: false,
    //   statusCallback: 'http://example.org',
    //   type: 'group-small',
    //   uniqueName: 'TestRoom2'
    // }).then(room=>{
    //   console.log(`ROMM: ${room.uniqueName} SID: ${room.sid}`)
    // })
    // client.video.rooms('TestRoom').fetch().then(room=>{
    //   console.log(`FETCHED ROOM: ${room.uniqueName}`)
    // }).catch(err=>{
    //   console.log(err)
    // })
    const { io } = args;
    io.of("/event").on("connection", (socket) => {
      socket.emit("connected", "you are connected to node event server");
      this._socket = socket;
      this.SOCKET_ID = socket.id;
      //this.initiateHandShake(io);
    });
  };

  // private initiateHandShake = (io: Server) => {
  //   this._socket.on(this.USER_JOIN_EVENT, (data) => {
  //     const { roomId, userId, stream } = data;
  //     //roomid and userid needs to be different
  //     //check if the room exists and user exists
  //     //check based on the room if the user is host
  //     const isHost = true || false; // get from sql
  //     this.usersInRoom = this.joinEventRoom(roomId, isHost);
  //     if (this.usersInRoom > 1) {
  //       this.broadcastStreamInChannel(roomId, stream, this.SOCKET_ID, io);
  //       this._socket.on(this.USER_STREAM_EVENT, (data) => {
  //         const { usersStream, socketIdToEmit } = data;
  //         this.emitStream(usersStream, socketIdToEmit);
  //       });
  //     }
  //     this._socket.on(this.LEAVE_ROOM_EVENT, (roomToLeave) => {
  //       this.usersInRoom = this.leaveEventRoom(roomToLeave);
  //     });
  //   });
  // };

  // private joinEventRoom = (roomId: string, isHost: boolean): number => {
  //   this._socket.join(roomId);
  //   this._socket.emit(this.SUCCESS_JOIN_EVENT, { isHost });
  //   // update sql row for this user to hasJoined=true
  //   // query all users count related to this room who have joined and return that count
  //   return this.usersInRoom;
  // };

  // private leaveEventRoom = (roomToLeave: string): number => {
  //   this._socket.leave(roomToLeave);
  //   this._socket.disconnect();
  //   // update sql row for this user to hasJoined=false
  //   return this.usersInRoom;
  // };

  // private broadcastStreamInChannel = (
  //   roomId: string,
  //   stream: any,
  //   socketId: string,
  //   io: Server
  // ) => {
  //   io.of("/event")
  //     .to(roomId)
  //     .emit(this.BROADCAST_EVENT, { roomId, socketId, stream });
  //   // update sql row for this user to hasJoined=false
  // };

  // private emitStream = (stream: any, socketId: string) => {
  //   //match socketId in clientapp to check if user is emmiting to right user or not
  //   // io.of("/event")
  //   // .to(socketId)
  //   // .emit(this.EMIT_STREAM_EVENT, { stream, socketIdOfUser: this.SOCKET_ID });
  //   this._socket.broadcast
  //     .to(socketId)
  //     .emit(this.EMIT_STREAM_EVENT, { stream, socketIdOfUser: this.SOCKET_ID });
  // };
}
