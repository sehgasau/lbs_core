import { IEmailData } from "../../interface/IEmailer";
import SmtpTransporter from "../../config/SmtpTransporter";
import TemplateView from "../../util/TemplateView";
import * as path from "path";
import { Subscriber } from "../../util/types/EventHandlerTypes";
import { IBaseEmailService } from "./BaseEmailService";

export type IUserEmailService = InstanceType<typeof UserEmailService>;

export class UserEmailService extends IBaseEmailService {
  constructor() {
    super("UserEmailService");
  }
  public sendConsumerActivationEmail: Subscriber = async (payload) => {
    const { client, email, username, activationUrl, tempPassword } = payload;

    const filePath = path.join(
      __dirname,
      "/../../views/email/consumerActivation.html"
    );
    const replacements = {
      name: username,
      client: client,
      email: email,
      activationUrl: activationUrl,
      tempPassword: tempPassword,
    };
    const template = TemplateView.compile(filePath, replacements);
    const emailData: IEmailData = {
      from: `admin <admin@mail.liteboards.com>`,
      to: email,
      subject: `${client} is requesting you to activate your account`,
      html: template, // take from templates,
      attachments: [
        {
          filename: "bulb.png",
          path: __dirname + "/../../resources/images/bulb.png",
          cid: "logo",
        },
      ],
    };
    await this.sendEmail(emailData);
  };

  public sendSupInvitationEmail: Subscriber = async (payload) => {
    const { host, invitees, eventUrl, eventDescription, eventDate } = payload;
    invitees.map(async (invitee) => {
      const { name, email } = invitee;
      const filePath = path.join(
        __dirname,
        "/../../views/email/supInvitationEmail.html"
      );
      const replacements = {
        name: name,
        host: host,
        email: email,
        eventUrl: eventUrl,
        eventDate: eventDate,
        eventDescription: eventDescription,
      };
      const template = TemplateView.compile(filePath, replacements);
      const emailData: IEmailData = {
        from: `admin <admin@mail.liteboards.com>`,
        to: email,
        subject: `${host} is inviting you to SUP event`,
        html: template, // take from templates,
        // attachments: [
        //   {
        //     filename: "bulb.png",
        //     path: __dirname + "/../../resources/images/bulb.png",
        //     cid: "logo",
        //   },
        // ],
      };
      await this.sendEmail(emailData);
    });
  };
}
