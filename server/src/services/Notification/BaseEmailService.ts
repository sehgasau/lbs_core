import { IEmailData } from "../../interface/IEmailer";
import SmtpTransporter from "../../config/SmtpTransporter";
import TemplateView from "../../util/TemplateView";
import * as path from "path";
import { Subscriber } from "../../util/types/EventHandlerTypes";
import Logger from "../../util/Logger/Logger";
import * as CONFIG from "../../config/config.mapper"

export abstract class IBaseEmailService {
  protected _context: string;
  protected _selfContext = "IBaseEmailService";
  private smtpTransporter: SmtpTransporter;
  private emailer: any;

  constructor(context: string) {
    this._context = context;
    this.smtpTransporter = new SmtpTransporter();
    this.emailer = this.smtpTransporter.initiateTransporter();
  }

  protected sendEmail = (emailData: IEmailData): void => {
    console.log(`Sending Email with context (${this._context})`);
    let message = "";
    this.emailer 
      .sendMail(emailData)
      .then((result) => {
        console.log("RESULT: ", result);
        message = "Email successfully sent";
        Logger.log({
          env: CONFIG["LBCONFIG_NODE_ENV"],
          context: this._context,
          level: "info",
          message:message,
          loggerType: "CONTEXT",
        });
      })
      .catch((err) => {
        message = `Email not sent with err - ${err.message}`;
        Logger.log({
          env: CONFIG["LBCONFIG_NODE_ENV"],
          context: this._context,
          level: "error",
          message,
          loggerType: "CONTEXT",
        });
      });
  };
}
