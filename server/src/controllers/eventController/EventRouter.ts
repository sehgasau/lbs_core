import * as express from "express";
import jwt from "jsonwebtoken";
import Response from "../../config/HttpResponses";
import Client from "../../entity/client/Client";
import { Connection, Like } from "typeorm";
import AuthMiddleware from "../middlewares/Auth";
import { EventController, IEventController } from "./EventController";
import TwilioConfig from "../../config/TwilioConfig";
import { Helper } from "../../util/index.helper";
import { BaseRouter } from "../BaseRouter";
import {
  IUserEmailService,
  UserEmailService,
} from "../../services/Notification/UserEmailService";
import * as CONFIG from "../../config/config.mapper";
import Logger from "../../util/Logger/Logger";
import Types from "../../util/types/index.types";

export default class EventRouter extends BaseRouter<IEventController> {
  private userEmailService: IUserEmailService;

  constructor(connection:Connection) {
    super(connection, "EventRouter");
    this.controller = new EventController(this.connection);
    this.userEmailService = new UserEmailService();
    this.initiateAllRoutes([
      this.getNewTwilioAccessToken,
      this.getActiveTwilioRoom,
      this.createEventRoom,
      this.testRoute
    ]);
  }

  /**
   *GET - user token to get into event room
   *@return  http response with {token,room} or error
   */
  private getNewTwilioAccessToken = () => {
    this.router.get(
      "/generateAccessToken/:identity/:room",
      async (req, res) => {
        const { room, identity } = req.params;
        const activeRoom = await this.controller.getRoom(room);
        const _room = activeRoom
          ? activeRoom
          : await this.controller.createTwilioRoom(room);
        if (!_room) {
          res.status(Response.ServerError.status).json({
            error: `${Response.ServerError.message}`,
          });
          return;
        }
        const token = this.controller.generateToken(identity, room);
        res.status(200).json({
          token: token,
          room: _room,
        });
      }
    );
  };

  /**
   *GET - active room
   *@return  http response with active room or error
   */
  private getActiveTwilioRoom = () => {
    this.router.get("/getRoom/:room", async (req, res) => {
      const { room } = req.params;
      const activeRoom = await this.controller.getRoom(room);
      if (!activeRoom) {
        res.status(Response.NotFound.status).json({
          error: `${Response.NotFound.message} - room not found (${room})`,
        });
        return;
      }
      res.status(200).json({
        room: activeRoom ? activeRoom : `Room not found : (${room})`,
      });
    });
  };

  /**
   *POST - create room
   *@return  room created or error
   */
  private createEventRoom = () => {
    this.router.post("/createEventRoom", async (req, res) => {
      this.controller._eventSupEventInserted.subscribers.push(
        this.userEmailService.sendSupInvitationEmail
      );
      try{
      const createdEventRoom = await this.controller.createEventModelRoom(req.body);
      if (!createdEventRoom) {
        this.controller._eventSupEventInserted._removeAllSubscribers()
        res.status(Response.ServerError.status).json({
          error: `${Response.ServerError.message} - error creating room (${createdEventRoom})`,
        });
        return;
      }
      res.status(200).json({
        room: createdEventRoom
          ? createdEventRoom
          : `Error creating room (${createdEventRoom}) `,
      });
    }
    catch(err){
      this.controller._eventSupEventInserted._removeAllSubscribers()
      res.status(500).json({
        error: `Error creating room - internal server error - ${err}`,
      });
    }
    });
    
  };


  private testRoute = () => {
    this.router.post("/testRoute",async (req, res) => {
     const invitee = await this.controller.testInviteeEventQuery(req.body);
     res.status(200).json({
      room: invitee
        ? invitee
        : `Error creating room (${invitee}) `, 
    });
    })
  }



  public getEventRoutes = () => {
    return this.router;
  };
}
