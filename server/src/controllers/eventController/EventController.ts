import User from "../../entity/user/User";
import Client from "../../entity/client/Client";
import IEventHandler from "../../interface/IEventHandler";
import EventHandler from "../../util/EventDelegation/EventHandler";
import { Like, getRepository, createQueryBuilder, Repository, Connection } from "typeorm";
import { filterOptions } from "../../util/types/UserTypes";
import Types from "../../util/types/index.types";
import TwilioConfig from "../../config/TwilioConfig";
import {
  RoomListInstanceCreateOptions,
  RoomRoomType,
} from "twilio/lib/rest/video/v1/room";
import EventRoom from "../../entity/eventRoom/EventRoom";
import { validate } from "class-validator";
import { Invitee, JoinStatus } from "../../entity/invitee/Invitee";
import * as cryptoRandomString from "crypto-random-string";
import { resolve } from "url";
import * as CONFIG from "../../config/config.mapper"

export type IEventController = InstanceType<typeof EventController>;
export class EventController {
  private _eventRoomRepo: Repository<EventRoom>;
  private _userRepo: Repository<User>;
  private _inviteeRepo: Repository<Invitee>;
  public _eventSupEventInserted: IEventHandler<object>;

  private connection:Connection;
  constructor(connection:Connection) {
    this.connection = connection;
    this._eventRoomRepo = this.connection.getRepository(EventRoom);
    this._userRepo = this.connection.getRepository(User);
    this._inviteeRepo = this.connection.getRepository(Invitee)
    this._eventSupEventInserted = new EventHandler();
  }

  public testInviteeEventQuery = async(payload)=>{
    console.log(payload)
    const invitee = await this._inviteeRepo
    .createQueryBuilder("invitee")
    .leftJoinAndSelect("invitee.eventRooms", "event")
    .where("event.eventId = :eventId",{eventId:payload.eventId})
    .andWhere("invitee.userId = :userId",{userId:payload.userId})
    .getOne();
    
    console.log(invitee)
    return invitee
  }

  public getRoom =async (room:string) =>{
    return await TwilioConfig.getRoom(room)
  }

  public createTwilioRoom =async (room:string) =>{
    const roomOptions: RoomListInstanceCreateOptions = {
      uniqueName: room,
      enableTurn: false,
      type: "group-small",
    };
    return await TwilioConfig.createRoom(roomOptions)
  }

  public generateToken = async(identity:string,room:string) =>{
    return await TwilioConfig.generateVideoAccessToken(identity,room)
  }

  public handleEventJoin = async (payload) => {
    // TODO: user has 1-M relation with invitee, so update schema entity 
    const {roomName,userId} = payload
    const eventUrl = `${CONFIG["LBCONFIG_SUP_EVENT_URL"]}/${roomName}`
    const event = await this._eventRoomRepo.findOne({eventUrl:eventUrl})

    if(!event || (event && Date.parse(event.eventDate.toDateString()) < Date.now())){
      throw "Event room doesn't exist or event date is not reached"
    } 
    const twilioRoom = await TwilioConfig.getRoom(roomName);
    if(!twilioRoom){
      const roomCreated = await this.createTwilioRoom(roomName)
      if(!roomCreated) throw "Error creating twilio room"
    }
    
    if(event.userId && userId){
      // this means event is internal, so perform invitee entity updates 

      // this query will return invitee for particular event
      const invitee = await this._inviteeRepo
        .createQueryBuilder("invitee")
        .leftJoinAndSelect("invitee.eventRooms", "event")
        .where("event.eventId = :eventId",{eventId:payload.eventId})
        .andWhere("invitee.userId = :userId",{userId:payload.userId})
        .getOne();

        if(!invitee) throw "Invitee not found"

        const newPayload = {
          joinStatus:JoinStatus.JOINED,
          hasLeftRoom:false,
          isViewable:true,
          isMuted:false
        }
      
        const isInviteeUpdated = await this._inviteeRepo.createQueryBuilder("invitee")
        .update<Invitee>(Invitee,newPayload)
        .where(`invitee.inviteeId = :inviteeId`, { inviteeId: invitee.inviteeId})
        .execute()
        if(!isInviteeUpdated) throw "Error updating Invitee"
    }
    const attendence = event.attendence + 1;
    const eventRoomUpdateResult = await this._eventRoomRepo.createQueryBuilder("eventRoom")
    .update<EventRoom>(EventRoom,{attendence:attendence})
    .where(`eventRoom.eventId = :eventId`, { eventId: event.eventId})
    .execute()
    if(!eventRoomUpdateResult) throw "Error updating EventRoom attendence"
    return eventRoomUpdateResult
  };

  public createEventModelRoom = async (
    payload: Types.EventRoomPayload
  ): Promise<any> => {
    const {
      eventType,
      userId,
      eventDate,
      location,
      from,
      to,
      invitees,
      eventDescription,
      eventCreatorName
    } = payload;

    const randomString = cryptoRandomString({
      length: 10,
      type: "url-safe",
    })
    // if the string includes dot then replace all the dots in the string 
    const newHash = randomString.includes('\.')? randomString.replace(/\./g,`S`):randomString
    const emailInviteePayload = [];
    let userHost;

    let eventToInsert = new EventRoom();
    eventToInsert.eventType = eventType;
    eventToInsert.location = location;
    eventToInsert.from = new Date();
    eventToInsert.to = new Date();
    // for non-client events the eventDate will be now()
    eventToInsert.eventDate = new Date();
    eventToInsert.eventUrl = `${CONFIG["LBCONFIG_SUP_EVENT_URL"]}${newHash}`;

    if (!!userId && !!invitees) {
      eventToInsert.userId = userId;
      eventToInsert.eventDescription = eventDescription;
      eventToInsert.totalInvitees = invitees? invitees.length + 1 : 0;

      userHost = await this._userRepo.findOne({ userId: userId });
      if(!userHost)
        throw "User who is trying to create the event, doesn't exist"

      // add the person who created this event
      let host = new Invitee();
      host.userId = userId;
      host.isHost = true;
      host.isInvitee = false;
      host.joinStatus = JoinStatus.INACTIVE;
      host.dateInvited = new Date();

      const isHostValidateOk = await validate(host);
      if(isHostValidateOk.length > 0) throw "Host validation failed"

      let inviteesToAdd: Invitee[] = [host];

      const addInvitees = () =>{
        return new Promise((resolve,reject)=>{
          // create invitees
          invitees.map(async (invitee,index) => {
            const {
              userId,
              isHost,
              isInvitee,
              dateInvited,
              isExternalInvitee,
            } = invitee;
            let inviteeToAdd = new Invitee();

            const inviteeToUser = await this._userRepo.findOne({
              userId: invitee.userId,
            });

            if (!!inviteeToUser) {
              emailInviteePayload.push({
                email: inviteeToUser["workEmail"],
                name: inviteeToUser["name"],
              });
            }

            inviteeToAdd.userId = userId;
            inviteeToAdd.isHost = isHost;
            inviteeToAdd.isInvitee = isInvitee;
            inviteeToAdd.dateInvited = new Date();
            inviteeToAdd.isExternalInvitee = false;
            inviteeToAdd.joinStatus = JoinStatus.INACTIVE;

            const isInviteeValidateOk = await validate(inviteeToAdd);

            // technically we should continue to add invitee if one invitee's validation is not ok then we should not stop other invitee's being added
            if (isInviteeValidateOk.length > 0)
              throw "Add Invitee Validation Failed"; // change to continue;

            // await this.connection.manager.save(inviteeToAdd);
            inviteesToAdd.push(inviteeToAdd);
            if(invitees.length === 0 || index === -1)reject()
            if(index === invitees.length-1)resolve()
          });
        })
      }

      if(invitees.length>0){
        await addInvitees()
        //await this.connection.manager.save(host);
        eventToInsert.invitees = inviteesToAdd;
      }
      
    }
      // validate the payload
    const isEventValidateOk = await validate(eventToInsert);
    if (isEventValidateOk.length > 0 ) {
      throw "Add Event Validation Failed";
    }
    return this.connection.manager
      .save(eventToInsert)
      .then(async (res) => {
        if(!!res.userId){
          const emailPayload = {
            invitees: emailInviteePayload,
            host: userHost.name,
            eventDate: new Date(res.eventDate).toDateString(),
            eventUrl: res.eventUrl,
            eventDescription: res.eventDescription,
          };
          this._eventSupEventInserted._raiseEvent(true, emailPayload);
      }
        return res;
      })
      .catch((err) => {
        console.log(err)
        this._eventSupEventInserted._removeAllSubscribers()
        return null;
      });
  };
}
