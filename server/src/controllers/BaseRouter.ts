import Types from "../util/types/index.types";
import * as express from "express";
import {
  IBranchController,
  BranchController,
} from "./branchController/BranchController";
import {
  IEventController,
  EventController,
} from "./eventController/EventController";
import { Helper } from "../util/index.helper";
import { Connection } from "typeorm";

export abstract class BaseRouter<T> {
  protected connection: Connection;
  protected controller: T;
  protected router: express.Router;
  private _selfContext = "BaseRouter";
  protected _context: string;

  constructor(connection: any, context: string) {
    this.router = express.Router();
    this.connection = connection;
    this._context = context;
  }

  protected initiateAllRoutes = (cbs: Function[]) => {
    Helper.initiateAllRoutes(cbs);
  };
}
