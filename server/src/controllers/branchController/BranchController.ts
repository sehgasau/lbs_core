import User from "../../entity/user/User";
import Client from "../../entity/client/Client";
import IEventHandler from "../../interface/IEventHandler";
import EventHandler from "../../util/EventDelegation/EventHandler";
import { Like, getRepository, createQueryBuilder, Connection } from "typeorm";
import { filterOptions } from "../../util/types/UserTypes";
import Branch from "../../entity/branch/Branch";

export type IBranchController = InstanceType<typeof BranchController>;

export class BranchController {
  // testing custom EventDelegation
  public _eventBranchInsert: IEventHandler<object>;
  private connection:Connection;
  constructor(connection:Connection) {
    this.connection = connection;
    this._eventBranchInsert = new EventHandler();
  }

  public findBranchesBasedOnClient = async (clientName: string) => {
    let query = await this.connection
      .getRepository(Branch)
      .createQueryBuilder("branch")
      .select(["branch.name"])
      .innerJoin("branch.client", "c")
      .where("c.name= :cname")
      .setParameters({ cname: clientName })
      .getMany();
    return query;
  };
}
