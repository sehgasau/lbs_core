import * as express from "express";
import jwt from "jsonwebtoken";
import Response from "../../config/HttpResponses";
import Client from "../../entity/client/Client";
import { UserEmailService } from "../../services/Notification/UserEmailService";
import { Connection, Like } from "typeorm";
import AuthMiddleware from "../middlewares/Auth";
import { BranchController, IBranchController } from "./BranchController";
import { Helper } from "../../util/index.helper";
import { BaseRouter } from "../BaseRouter";

export default class BranchRouter extends BaseRouter<IBranchController> {
  constructor(connection:Connection) {
    super(connection, "BranchRouter");
    this.controller = new BranchController(connection);
    this.initiateAllRoutes([this.getBranchesBasedOnClient]);
  }

  // GET - brances based on client
  private getBranchesBasedOnClient = () => {
    this.router.get(
      "/branches/:clientName",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      AuthMiddleware.verifyRole([
        "CLIENT_ADMIN",
        "SUPER_ADMIN",
        "BRANCH_ADMIN",
      ]),
      async (req, res) => {
        const { clientName } = req.params;
        const branches = await this.controller.findBranchesBasedOnClient(
          clientName
        );
        res.status(200).json({
          branches: branches,
        });
      }
    );
  };

  public getBranchRoutes = () => {
    return this.router;
  };
}
