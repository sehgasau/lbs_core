import * as jwt from "jsonwebtoken";
import Response from "../../config/HttpResponses";
import { resolveSoa } from "dns";
import Types from "../../util/types/index.types";

export default class AuthMiddleware {
  public static ensureToken = (req, res, next) => {
    const bearerHeader = req.headers.authorization;
    if (typeof bearerHeader !== "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      req.token = bearerToken;
      next();
    } else {
      res.status(Response.Forbidden.status).json({
        message: Response.Forbidden.message,
      });
    }
  };

  public static verifyRole = (roles: Types.UserRoles) => {
    return (req, res, next) => {
      const roleHeader = req.headers.role;
      let isRoleVerified = false;
      roles.map((role) => {
        if (roleHeader === role) {
          isRoleVerified = true;
        }
      });
      if (isRoleVerified) {
        next();
      } else {
        res.status(Response.Forbidden.status).json({
          message: Response.Forbidden.message,
        });
      }
    };
  };

  // Verifies the validity of a jwt
  public static verifyToken = (req, res, next) => {
    jwt.verify(
      req.token,
      "asdasddas",
      { issuer: "liteboardscom" },
      (err, data) => {
        if (err) {
          res.status(Response.Forbidden.status).json({
            message: err.message,
            at: err.expiredAt,
          });
        } else {
          req.user = data;
          next();
        }
      }
    );
  };

  public static signToken = (payload) => {
    const tokenOptions = {
      algorithm: "HS256",
      expiresIn: "1 hour",
      issuer: "liteboardscom",
    };

    return jwt.sign(payload, "asdasddas", tokenOptions);
  };
}

// Ensures the existence of a jwt
