import User from "../../entity/user/User";
import Client from "../../entity/client/Client";
import IEventHandler from "../../interface/IEventHandler";
import EventHandler from "../../util/EventDelegation/EventHandler";
import { Like, Connection, Repository } from "typeorm";
import { v4 as uuidv4 } from "uuid";
import { validate } from "class-validator";

export type IClientController = InstanceType<typeof ClientController>;
export class ClientController {
  // testing custom EventDelegation
  public _eventClientInsert: IEventHandler<object>;
  private connection:Connection;
  private _clientRepo: Repository<Client>;

  constructor(connection: Connection) {
    this.connection = connection;
    this._eventClientInsert = new EventHandler();
    this._clientRepo = this.connection.getRepository(Client);
  }

  // GET - Client by user
  public findClientByUserId = () => {};

  // GET - All clients
  public findAllClients = async () => {
    const query = await this.connection
      .getRepository(Client)
      .createQueryBuilder("client")
      .select([
        "client.name",
        "client.industryType",
        "client.subscriptionType",
        "client.creationDate",
      ])
      .getMany();

    return query;
  };

  // GET - client by name
  public findClientByName = async (clientName: string) => {
    const query = await this.connection
      .getRepository(Client)
      .createQueryBuilder("client")
      .select([
        "client.clientId",
        "client.industryType",
        "client.subscriptionType",
        "client.name",
      ])
      .where({
        name: clientName,
      })
      .getOne();

    return query;
  };

  public addClientToDb = async (payload) => {
    const {name,subsType,industry } = payload;
    try {
      const client = await this._clientRepo.findOne({
        where: {
          name: name.toString(),
        },
      });
      if (client) {
        throw "Client already Exists";
      }
      const clientId = uuidv4();
      const clientToInsert = new Client();
      clientToInsert.name = name;
      clientToInsert.role = "CLIENT";
      clientToInsert.industryType = industry;
      clientToInsert.subscriptionType = subsType;
      clientToInsert.clientId = clientId;

      const isValidateOk = await validate(clientToInsert);
      if (isValidateOk.length > 0) {
        throw "Validation Failed";
      }
      return this._clientRepo.save(clientToInsert).then(async (client) => {
        return client;
      });
    } catch (err) {
      return Promise.reject(err);
    }
  };

}
