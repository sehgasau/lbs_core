import * as express from "express";
import jwt from "jsonwebtoken";
import Response from "../../config/HttpResponses";
import Client from "../../entity/client/Client";
import {
  UserEmailService,
  IUserEmailService,
} from "../../services/Notification/UserEmailService";
import { Connection, Like } from "typeorm";
import { ClientController, IClientController } from "./ClientController";
import { Helper } from "../../util/index.helper";
import { BaseRouter } from "../BaseRouter";
import AuthMiddleware from "../middlewares/Auth";

export default class ClientRouter extends BaseRouter<IClientController> {
  constructor(connection:Connection) {
    super(connection, "ClientRouter");
    this.controller = new ClientController(this.connection);
    this.initiateAllRoutes([
      this.getAllClients,
      this.addClient,
      this.sendEmailTest,
    ]);
  }

  // for testing - TMP -> WORKING
  private sendEmailTest = () => {
    this.router.get("/emailtest", async (req, res) => {
      const userEmailer = new UserEmailService();
      const payload = {
        client: "Scotiabank",
        username: "Saurav",
        activationUrl:
          "http://dev.liteboards.com/user/activate/44298c1e-c564-11ea-a565-525400e661d4",
        email: "ssehgal@grapevine6.com",
      };
      await userEmailer.sendConsumerActivationEmail(payload);
      console.log("now print this");
    });
  };

  /**
   * POST - add client to db (only super admin is allowed to do this)
   * @Return  http response with inserted client
   * @param  void
   */
  private addClient = () => {
    this.router.post(
      "/add-client",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      AuthMiddleware.verifyRole(["SUPER_ADMIN"]),
      async (req, res) => {
        try {
          const insertedClient = await this.controller.addClientToDb(req.body);
          return res
            .status(Response.OK.status)
            .json({ message: Response.OK.message, client: insertedClient });
        } catch (err) {
          return res.status(Response.ServerError.status).json({
            message: Response.ServerError.message,
            serverMessage: err,
          });
        }
      }
    );
  };

  // GET - All Clients
  private getAllClients = () => {
    this.router.get("/clients", async (req, res) => {
      const clients = await this.controller.findAllClients();
      res.send(clients);
    });
  };

  public getClientRoutes = () => {
    return this.router;
  };
}
