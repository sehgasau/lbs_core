import * as express from "express";
import ClientRouter from "./clientController/ClientRouter";
import UserRouter from "./userController/UserRouter";
import BranchRouter from "./branchController/BranchRouter";
import EventRouter from "./eventController/EventRouter";

import { Connection } from "typeorm";
import Types from "../util/types/index.types";

export default class RootRouter {
  private rootRouter: any;
  private connection: Connection;
  constructor(connection: Connection) {
    this.connection = connection;
    this.rootRouter = express.Router();
  }

  private initAllRoutes = (cbs: Types.CallbackArray) => {
    if (cbs && cbs.length > 0) {
      cbs.map((cbObject) => {
        this.rootRouter.use(`${cbObject.route}`, cbObject.cb());
      });
    }
  };

  public getRoutes = () => {
    this.initAllRoutes([
      {
        route: "/client",
        cb: new ClientRouter(this.connection).getClientRoutes,
      },
      {
        route: "/user",
        cb: new UserRouter(this.connection).getuserRoutes,
      },
      {
        route: "/branch",
        cb: new BranchRouter(this.connection).getBranchRoutes,
      },
      {
        route: "/event",
        cb: new EventRouter(this.connection).getEventRoutes,
      },
    ]);

    return this.rootRouter;
  };
}
