import User from "../../entity/user/User";
import Client from "../../entity/client/Client";
import IEventHandler from "../../interface/IEventHandler";
import EventHandler from "../../util/EventDelegation/EventHandler";
import { Like, getRepository, createQueryBuilder, Repository, Connection } from "typeorm";
import { filterOptions, userInsertPayload } from "../../util/types/UserTypes";
import { connect } from "net";
import PasswordUtil from "../../util/PasswordUtil/PasswordUtil";
import { v4 as uuidv4 } from "uuid";
import Response from "../../config/HttpResponses";
import { validate } from "class-validator";
import * as CONFIG from "../../config/config.mapper"
export type IUserController = InstanceType<typeof UserController>;
export class UserController {
  // testing custom EventDelegation
  public _eventUserInsert: IEventHandler<object>;
  private connection:Connection;
  private _userRepo: Repository<User>;
  private _clientRepo: Repository<Client>;

  constructor(connection:Connection) {
    this.connection = connection;
    this._eventUserInsert = new EventHandler();
    this._userRepo = this.connection.getRepository(User);
    this._clientRepo = this.connection.getRepository(Client);
  }

  public findUserByEmail = async (email: string) => {
    const user = await this._userRepo
      .createQueryBuilder("user")
      .where("user.workEmail = :email", { email: email })
      .getOne();
    return user;
  };

  public findUserById = async (userId: string) => {
    const user = await this._userRepo
      .createQueryBuilder("user")
      .where("user.userId = :userId", { userId: userId })
      .getOne();
    return user;
  };

  public finduserAndItsClient = async (userId: string) => {
    const clientAndItsUser = await this._userRepo
      .createQueryBuilder("u")
      .select([
        "u.name",
        "u.role",
        "u.workEmail",
        "c.name",
        "u.userId",
        "u.isActivated",
      ])
      .innerJoin("u.client", "c")
      .where("u.userId = :userId", { userId: userId })
      .getOne();
    return clientAndItsUser;
  };

  public updateUserActivationCreds = async (userId: string) => {
    const date = new Date();
    const result = await this._userRepo
      .createQueryBuilder()
      .update(User)
      .set({ isActivated: true, activationDate: date })
      .where("userId = :userId", { userId: userId })
      .execute();

    return result;
  };

  public addUserToDb = async (payload) => {
    const { workEmail, name, clientName, branchName, role } = payload;
    try {
      const tempPassword: string = PasswordUtil.generatePassword();
      const encryptedTempPassword: string = await PasswordUtil.encryptPassword(
        tempPassword
      );
      const client = await this._clientRepo.findOne({
        where: {
          name: clientName.toString(),
        },
      });
      if (!client) {
        throw "Client Not Found";
      }
      const userId = uuidv4();
      const userToInsert = new User();
      userToInsert.name = name;
      userToInsert.password = encryptedTempPassword;
      userToInsert.workEmail = workEmail;
      userToInsert.clientId = client.clientId;
      userToInsert.role = role;
      userToInsert.userId = userId;

      const isValidateOk = await validate(userToInsert);
      if (isValidateOk.length > 0) {
        throw "Validation Failed";
      }

      return this._userRepo.save(userToInsert).then(async (user) => {
        const insertedUser = user;
        // emailer payload
        const s = {
          client: client.name,
          username: insertedUser.name,
          activationUrl:
          CONFIG["LBCONFIG_REACT_URL"] + "/user/activate/" + insertedUser.userId,
          email: insertedUser.workEmail,
          tempPassword: tempPassword,
        };
        this._eventUserInsert._raiseEvent(true, s);
        return Response.OK.status;
      });
    } catch (err) {
      this._eventUserInsert._removeAllSubscribers();
      return Promise.reject(err);
    }
  };

  public findUsersWithOptionsByClient = async (options: filterOptions) => {
    let query = await this._userRepo
      .createQueryBuilder("user")
      .select([
        "user.designation",
        "user.name",
        "user.workEmail",
        "b.name",
        "user.isActivated",
        "c.name",
      ])
      .leftJoin("user.branch", "b")
      .innerJoin("user.client", "c")
      .where(
        options.isActivated === null
          ? {
              role: options.role,
            }
          : {
              isActivated: options.isActivated,
              role: options.role,
            }
      );
    if (options.cName !== null) {
      query = query.andWhere("c.name= :cname");
    }
    if (options.userName && options.userName.trim() !== "") {
      query = query.andWhere(`user.name like "%${options.userName}%"`);
    }
    if (options.branchName && options.branchName.trim() !== "") {
      query = query.andWhere("b.name= :branchName");
    }
    const result = await query
      .offset(options.offset)
      .limit(options.limit)
      .setParameters({
        branchName: options.branchName ? options.branchName : "",
        cname: options.cName,
      })
      .getMany();
    return result;
  };
}
