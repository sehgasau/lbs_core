import Response from "../../config/HttpResponses";
import AuthMiddleware from "../middlewares/Auth";
import { UserController, IUserController } from "./UserController";
import {
  UserEmailService,
  IUserEmailService,
} from "../../services/Notification/UserEmailService";
import { Connection } from "typeorm";
import PasswordUtil from "../../util/PasswordUtil/PasswordUtil";
import { IBaseEmailService } from "../../services/Notification/BaseEmailService";
import { Helper } from "../../util/index.helper";
import { BaseRouter } from "../BaseRouter";

export default class UserRouter extends BaseRouter<IUserController> {
  private SALTROUND = 10;
  private userEmailService: IUserEmailService;

  constructor(connection: Connection) {
    super(connection, "UserRouter");
    this.controller = new UserController(this.connection);
    this.userEmailService = new UserEmailService();
    this.initiateAllRoutes([
      this.getAllUsers,
      this.authenticateUser,
      this.getUserByEmail,
      this.getUserById,
      this.insertUser,
      this.getUserVerificationStatus,
      this.updateUserActivationStatusAndDate,
      this.getUserAndItsClientByUserId,
      this.getUsersByClientAndOptions,
    ]);
  }

  //--------------------------------------------------------------------------------

  // GET - All Users
  private getAllUsers = () => {
    this.router.get("/", AuthMiddleware.ensureToken, (req, res) => {
      res.send("All USERS NEW blueGreen");
    });
  };

  // GET - User by email
  private getUserByEmail = () => {
    this.router.get(
      "/single-user",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      async (req, res) => {
        const { email } = req.body
        const user = await this.controller.findUserByEmail(email.toString());
        const payload = {
          userId: user.userId,
          email: user.workEmail,
          role: user.role,
        };
        res.status(200).json({
          user: payload,
        });
      }
    );
  };

  // GET - User by id
  private getUserById = () => {
    this.router.get(
      "/single-user/:userId",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      async (req, res) => {
        const { userId } = req.params;
        const user = await this.controller.findUserById(userId.toString());
        const payload = {
          userId: user.userId,
          email: user.workEmail,
          role: user.role,
        };
        res.status(200).json({
          user: payload,
        });
      }
    );
  };

  // GET - User and its client by id
  private getUserAndItsClientByUserId = () => {
    this.router.get(
      "/single-userAndClient/:userId",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      async (req, res) => {
        const { userId } = req.params;
        const user = await this.controller.finduserAndItsClient(
          userId.toString()
        );
        const payload = {
          userId: user.userId,
          email: user.workEmail,
          role: user.role,
          client: user.client.name,
          isActivated: user.isActivated,
        };
        res.status(200).json({
          user: payload,
        });
      }
    );
  };

  // GET - User and its client by id
  private getUsersByClientAndOptions = () => {
    this.router.get(
      "/usersByOptions",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      AuthMiddleware.verifyRole([
        "CLIENT_ADMIN",
        "SUPER_ADMIN",
        "BRANCH_ADMIN",
      ]),
      async (req, res) => {
        const { options  } = req.query;
        const users = await this.controller.findUsersWithOptionsByClient(
          JSON.parse(options as string)
        );
        res.status(200).json({
          users: users,
        });
      } 
    );
  };

  // GET - Verify user activation and return user if all good
  private getUserVerificationStatus = () => {
    this.router.get("/activation/status/:userId", async (req, res) => {
      const { userId } = req.params;
      const userAndItsClient = await this.controller.finduserAndItsClient(
        userId.toString()
      );
      if (!userAndItsClient) {
        return res.status(Response.Unauthorized.status).json({
          message: Response.Unauthorized.message,
        });
      }
      return res.status(Response.OK.status).json({
        isActivated: userAndItsClient.isActivated,
        userName: userAndItsClient.name,
        client: userAndItsClient.client.name,
      });
    });
  };

  // PUT - Change user's activation status and activation date
  private updateUserActivationStatusAndDate = () => {
    this.router.put("/activation/update/:userId", async (req, res) => {
      const { userId } = req.params;
      const result = await this.controller.updateUserActivationCreds(
        userId.toString()
      );
      if (!result) {
        return res.status(Response.Unauthorized.status).json({
          message: Response.Unauthorized.message,
        });
      }
      return res.status(Response.OK.status).json({
        isActivated: true,
      });
    });
  };

  // POST - Auth User
  private authenticateUser = () => {
    this.router.post("/authenticate", async (req, res) => {
      const { email, password, isAdminLoginAttempt } = req.body;
      const user = await this.controller.findUserByEmail(email.toString());
      if (!user) {
        return res
          .status(Response.Forbidden.status)
          .json({ message: Response.Forbidden.message });
      }
      if (
        isAdminLoginAttempt &&
        user.role !== "CLIENT_ADMIN" &&
        user.role !== "SUPER_ADMIN"
      ) {
        return res
          .status(Response.Forbidden.status)
          .json({ message: Response.Forbidden.message });
      } else if (!isAdminLoginAttempt && user.role !== "CONSUMER") {
        return res
          .status(Response.Forbidden.status)
          .json({ message: Response.Forbidden.message });
      }

      const isPassowrdCorrect: boolean = await PasswordUtil.comparePassword(
        password.toString(),
        user.password.toString()
      );
      if (!isPassowrdCorrect) {
        console.log("incorrect password");
        return res
          .status(Response.Forbidden.status)
          .json({ message: Response.Forbidden.message });
      }
      const payload = {
        userId: user.userId,
        email: user.workEmail,
        role: user.role,
      };
      const token = AuthMiddleware.signToken(payload);
      return res
        .status(Response.OK.status)
        .json({ token, isActivated: user.isActivated });
    });
  };

  // POST - Insert User
  private insertUser = () => {
    this.router.post(
      "/add-user",
      AuthMiddleware.ensureToken,
      AuthMiddleware.verifyToken,
      AuthMiddleware.verifyRole([
        "CLIENT_ADMIN",
        "SUPER_ADMIN",
        "BRANCH_ADMIN",
      ]),
      (req, res) => {
        this.controller._eventUserInsert.subscribers.push(
          this.userEmailService.sendConsumerActivationEmail
        );
        this.controller
          .addUserToDb(req.body)
          .then((response) => {
            return res.status(response).json({ message: Response.OK.message });
          })
          .catch((err) => {
            this.controller._eventUserInsert._removeAllSubscribers();
            return res.status(Response.ServerError.status).json({
              message: Response.ServerError.message,
              serverMessage: err,
            });
          });
      }
    );
  };

  public getuserRoutes = () => {
    return this.router;
  };
}
