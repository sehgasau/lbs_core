import * as TW from "twilio";
import {
  RoomInstance,
  RoomListInstanceCreateOptions,
  RoomListInstanceOptions,
} from "twilio/lib/rest/video/v1/room";
import { AccessTokenOptions, VideoGrant } from "twilio/lib/jwt/AccessToken";
const CONFIG = require("../config/config.mapper")

export default class TwilioConfig {
  private static TW_INSTANCE: TW.Twilio;
  public static connectApi = () => {
    TwilioConfig.TW_INSTANCE = TW(
      CONFIG["LBCONFIG_TWILIO_SID"],
      CONFIG["LBCONFIG_TWILIO_AUTH_TOKEN"]
    );
  };

  public static generateVideoAccessToken = (
    identity: string,
    roomName: string
  ): string => {
    const tokenOptions: AccessTokenOptions = {
      identity: identity,
    };
    const token = new TW.jwt.AccessToken(
      CONFIG["LBCONFIG_TWILIO_SID"],
      CONFIG["LBCONFIG_TWILIO_API_KEY"],
      CONFIG["LBCONFIG_TWILIO_API_SECRET"],
      tokenOptions
    );
    const videoGrant: VideoGrant = new TW.jwt.AccessToken.VideoGrant({
      room: roomName,
    });
    token.addGrant(videoGrant);
    return token.toJwt();
  };

  public static createRoom = (
    options: RoomListInstanceCreateOptions
  ): Promise<RoomInstance> => {
    return TwilioConfig.TW_INSTANCE.video.rooms
      .create(options)
      .then((room) => {
        return room;
      })
      .catch((err) => {
        return null;
      });
  };

  public static getRoom = (roomSid: string): Promise<RoomInstance> => {
    return TwilioConfig.TW_INSTANCE.video
      .rooms(roomSid)
      .fetch()
      .then((room) => {
        return room;
      })
      .catch((err) => {
        return null;
      });
  };

  public static closeRoom = (roomSid: string): Promise<RoomInstance> => {
    return TwilioConfig.TW_INSTANCE.video
      .rooms(roomSid)
      .update({ status: "completed" })
      .then((room) => {
        return room;
      })
      .catch((err) => {
        return err;
      });
  };

  public static updateRoom = (
    propertyObject: any,
    roomSid: string
  ): Promise<RoomInstance> => {
    return TwilioConfig.TW_INSTANCE.video
      .rooms(roomSid)
      .update(propertyObject)
      .then((room) => {
        return room;
      })
      .catch((err) => {
        return err;
      });
  };

  public static getAllRooms = (
    propertyObject: RoomListInstanceOptions
  ): Promise<Array<RoomInstance>> => {
    return TwilioConfig.TW_INSTANCE.video.rooms
      .list(propertyObject)
      .then((rooms) => {
        return rooms;
      })
      .catch((err) => {
        return err;
      });
  };
}
