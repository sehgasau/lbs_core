import * as path from "path";
import * as dotenv from "dotenv";

const configMapperFunction = ()=>{
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "local")
        dotenv.config({ path: path.join(__dirname, "../../../.env.local") });
    else if (process.env.NODE_ENV === "development")
        dotenv.config({ path: path.join(__dirname, "../../../.env.dev") });
    else if(process.env.NODE_ENV === "uat")
        dotenv.config({ path: path.join(__dirname, "../../../.env.uat") });
    else if(process.env.NODE_ENV === "test")
        dotenv.config({ path: path.join(__dirname, "../../../.env.test") });
    else
        dotenv.config({ path: path.join(__dirname, "../../../.env.prod") });

    let CONFIG={};
    Object.keys(process.env).map(key=>{
        const isLbConfigKey = key.toString().includes("LBCONFIG_")
        if(isLbConfigKey){
            CONFIG[key] = process.env[key]
        }
    })
    return CONFIG
}

const CONFIG = configMapperFunction()

module.exports = CONFIG