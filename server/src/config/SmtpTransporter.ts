import * as nodemailer from 'nodemailer';
import * as smtpTransport from 'nodemailer-smtp-transport';
import {ISmtpTransporter} from '../interface/IEmailer';
const CONFIG = require("../config/config.mapper")

export default class SmtpTransporter implements ISmtpTransporter {

    public initiateTransporter = () =>{

        const transporter = nodemailer.createTransport(smtpTransport({
            name: CONFIG["LBCONFIG_TRANSPORTER_NAME"],
            host: CONFIG["LBCONFIG_TRANSPORTER_HOST"],
            port: CONFIG["LBCONFIG_TRANSPORTER_PORT"],
            secure: true, // true for 465, false for other ports
            auth: {
            user: CONFIG["LBCONFIG_TRANSPORTER_USER"], // generated ethereal user
            pass: CONFIG["LBCONFIG_TRANSPORTER_PASSWORD"], // generated ethereal password
            },
            tls:{
            rejectUnauthorized:false
            }
        }));
        return transporter
    }
}

