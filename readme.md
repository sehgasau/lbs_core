# Liteboards SaaS

## Overview

### Liteboards is a smart PEO (People, Organization, and Event) management SaaS platform currently targeting small businesses across GTA. Some of the core features include traditional HR management services, Peer Video Conferencing and broadcasting, PayStub Management, Gratitude (Clever Front-Desk assistant), and the list goes on ....

## Project (prerequisite) Infrastructure and technologies

### Languages:

1. Typescript
2. Javascript
3. SQL

### Frameworks:

1. Typeorm (typescript framework to manage object relation mapping to Relational DB)
2. Google Material Design (Front-End UI framework included in react project to template the initial UI design)
3. Webpack (Build optimization and deployment)
4. Git
5. nodeJS and NPM
6. Some of the Back-End Frameworks: Express, Twilio, node-mailer, .....

### Tools:

1. VS-Code
2. MySQL WorkBench
3. Postman
4. Putty

### Repo Infrastructure:

#### Repo is modularized in a way that it includes Front-End (clientapp) and Back-End (Server) in the same project structure

#### clientapp: It is the front-end (User application and Admin Dashboard) of the application which holds the various UI components coded in react (JS)

#### server: It holds all the server-side logic, REST-API, Real-Time services to support UI

## Dev Setup

1. Clone the repo in your local machine
2. Open the project in VS-Code and open the Terminal
3. Run the following command in the Terminal while your pwd is root of the project : `npm install`
4. "node_modules" folder should be created at the root which will hold all the project dependencies
5. Create 4 files at the root : .env.local, .env.dev, .env.test, .env.uat, .env.prod
6. Copy the content from .envsample to all the above created files and ask the admin to provide all the required creds for the content copied (those are the Config variables injested in the local env for secutiry purposes)
7. After all the creds are written in the above created files, open Terminal and change the pwd to project root
8. Execute the following command in the terminal: `npm run local:watch`
9. Navigate to `http://localhost:8081` in the browser and boom u got it working
10. Server should be running at `http://localhost:3000`
